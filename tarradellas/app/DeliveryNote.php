<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryNote extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'delivery-notes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'user', 'date', 'customer_id', 'dealer_id', 'carrier_id', 'product_id', 'boxes', 'units', 'promotion', 'lot','observations','delivery_note_id','week','order_date','order_id','company_id','file_id','created_at','updated_at', 'validated','send'];

    
    public function customers()
    {
        return $this->hasOne('App\CustomerDealer', 'customer_id', 'customer_id');
    }

    public function users()
    {
        return $this->hasOne('App\User', 'company_id', 'company_id');
    }


}