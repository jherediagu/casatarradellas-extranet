<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductLogistic extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products_logistic_data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','product_id','name','family_id','ean_code','dun_code','dot_green','pallet_box','pallet_box_layer','pallet_layer','pallet_height','box_tara','box_units','box_units_pack','box_long','box_width','box_height','box_net_weight','box_gross_weight','unit_long','unit_width','unit_height','unit_diameter','unit_net_weight','unit_gross_weight','expiration'];


    public function products()
    {
        return $this->hasOne('App\Product', 'product_id', 'product_id');
    }


    public function families()
    {
        return $this->hasOne('App\Family', 'family_id', 'family_id');
    }

}




