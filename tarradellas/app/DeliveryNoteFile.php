<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryNoteFile extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'delivery-notes-files';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'user_id', 'import_date', 'file' , 'file_rows', 'company_id'];


    public function users()
    {
        return $this->hasOne('App\User', 'company_id', 'company_id');
    }
}