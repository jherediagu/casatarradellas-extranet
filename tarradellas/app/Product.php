<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','product_id','name','comercial_name','comercial_name_change','review_date','date','package_type','package_type_change','conservation','conservation_change','lot_expiration','lot_expiration_change','caract_prod','caract_prod_change','ingredients','ingredients_change','ingredients_b','ingredients_b_change','ingredients_g','ingredients_g_change','valor_energetic','valor_energetic_change','proteines','proteines_change','hidrats_carboni','hidrats_carboni_change','grasas','grasas_change','caract_biolog','caract_biolog_change','us_esperat','us_esperat_change','grup_consum','grup_consum_change','leaving_date','user','grasas_saturadas','grasas_saturadas_change','hidrats_carb_azucar','logisticshidrats_carb_azucar_change','sal','sal_change'];


    public function logistics()
    {
        return $this->hasOne('App\ProductLogistic', 'product_id', 'product_id');
    }

}


