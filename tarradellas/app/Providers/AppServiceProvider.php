<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\RelationNewsDealer as RelationNews;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $relation = RelationNews::all();
        view()->share('relation', $relation);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
