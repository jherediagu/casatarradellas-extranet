<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerDealer extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'customer-dealers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','dealer_id','customer_id','name','comercial_name','address','postal_code','city','province','NIF','carrier_id','carrier_address','carrier_postal_code','carrier_city','carrier_province','carrier_name', 'created_at', 'updated_at'];

}
