<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelationNewsDealer extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'relation_news_dealers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'id_news', 'id_dealer', 'viewed', 'updated_at', 'created_at'];


     public function relationnew()
    {
        return $this->hasOne('App\News', 'id', 'id_news');
    }

    public function dealer()
    {
        return $this->hasOne('App\Dealer', 'id', 'id');
    }
}





