<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\News as News;
use App\Dealer as Dealer;

use App\RelationNewsDealer as NewsDealer;

use Auth;


class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
  
        $news       = News::all();
        $news_total = News::count();

        return view('news.index', [ 'news' => $news, 
                                         'total' => $news_total]);

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexDealer()
    {
  
        $news = NewsDealer::where('id_dealer', Auth::user()->id)
                            ->get();

        $news_total = NewsDealer::where('id_dealer', Auth::user()->id)
                                    ->count();

        return view('news.index-dealer', [ 'news' => $news, 
                                         'total' => $news_total]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $news_data = $request->except('files');

        $news = new News($news_data);

        $news->save();

        $request->session()->flash('success', 'Noticia creada correctamente!');
        return redirect('news');    
    }

    /**
     * Display the specified resource. Show News for Dealer
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

     /**
     * Display the specified resource. Show News for Dealer
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showNewsDealer($id)
    {
        NewsDealer::where('id_dealer', Auth::user()->id)
                            ->where('id_news', $id)
                            ->update(['viewed' => 1]);

        $news = News::where('id', $id)->first();

        return view('news.show', [ 'news' => $news ]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news           = News::where('id', $id)->first();
        $dealers        = Dealer::all();
        $news_relations = NewsDealer::where('id_news',$id)->lists('id_dealer', 'id_dealer')->all();

        return view('news.edit', [ 'news'    => $news, 
                                  'dealers'  => $dealers,
                                  'relation' => $news_relations ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->has('selection_dealers')) {
            
            NewsDealer::where('id_news', $id)->delete();

            $selection_dealers = $request->input('selection_dealers');

            foreach ($selection_dealers as $dealer) {
                    
                $relations_news = new NewsDealer();
                $relations_news->id_news = $id;
                $relations_news->id_dealer = $dealer;
                $relations_news->save();

            }
        }

        $news_data = $request->except('files','check','id','sender','name','date','time', 'selection_dealers');

        $news = News::find($id);

        $news->fill($news_data);

        $news->save();

        $request->session()->flash('success', 'Noticia editada correctamente!');
        return redirect('news');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete row

        NewsDealer::where('id_news', $id)->delete();
        News::where('id', $id)->delete();
        return back();
    }
}
