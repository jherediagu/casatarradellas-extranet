<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Product as Product;
use App\Family  as Family;

use PDF;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource --> Products.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products       = Product::all();
        $families       = Family::all();
        $total_products = Product::count();

        return view('products.index', [ 'products' => $products,
                                        'families' => $families,
                                        'total'    => $total_products ]);
    }


    /**
     * Display a listing of the resource --> Products.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexDataSheet()
    {
        $products       = Product::all();
        $total_products = Product::count();

        return view('products.data_sheet.index', [  'products' => $products, 
                                                       'total' => $total_products ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file       = $request->file('products');
        $file_name  = $request->file('products')->getClientOriginalName();
        $extension  = $file->guessExtension();

        if ($extension != 'txt') {
            
            $request->session()->flash('error', 'El archivo no es correcto, no es un archivo txt');
            return redirect('products');
        }

        foreach( file($file) as $line) {

            $line = mb_convert_encoding($line, 'iso-8859-1', 'UTF-8');            
            $line_components = explode('|', $line);


            $id                         = $line_components[0];
            $product_id                 = $line_components[1];
            $name                       = $line_components[2];
            $comercial_name             = $line_components[3];
            $comercial_name_change      = $line_components[4];
            $review_date                = $line_components[5] != 0 ? date('Y-m-d',strtotime(str_replace('/', '-', $line_components[5]))) : '0';
            $date                       = $line_components[6] != 0 ? date('Y-m-d',strtotime(str_replace('/', '-', $line_components[6]))) : '0';
            $package_type               = $line_components[7];
            $package_type_change        = $line_components[8];
            $conservation               = $line_components[9];
            $conservation_change        = $line_components[10];
            $lot_expiration             = $line_components[11];
            $lot_expiration_change      = $line_components[12];
            $caract_prod                = $line_components[13];
            $caract_prod_change         = $line_components[14];
            $ingredients                = $line_components[15];
            $ingredients_change         = $line_components[16];
            $ingredients_b              = $line_components[17];
            $ingredients_b_change       = $line_components[18];
            $ingredients_g              = $line_components[19];
            $ingredients_g_change       = $line_components[20];
            $valor_energetic            = $line_components[21];
            $valor_energetic_change     = $line_components[22];
            $proteines                  = $line_components[23];
            $proteines_change           = $line_components[24];
            $hidrats_carboni            = $line_components[25];
            $hidrats_carboni_change     = $line_components[26];
            $grasas                     = $line_components[27];
            $grasas_change              = $line_components[28];
            $caract_biolog              = $line_components[29];
            $caract_biolog_change       = $line_components[30];
            $us_esperat                 = $line_components[31];
            $us_esperat_change          = $line_components[32];
            $grup_consum                = $line_components[33];
            $grup_consum_change         = $line_components[34];
            $leaving_date               = $line_components[35] != 0 ? date('Y-m-d',strtotime(str_replace('/', '-', $line_components[35]))) : '0';
            $user                       = $line_components[37];
            $grasas_saturadas           = $line_components[38];
            $grasas_saturadas_change    = $line_components[39];
            $hidrats_carb_azucar        = $line_components[40];
            $hidrats_carb_azucar_change = $line_components[41];
            $sal                        = $line_components[42];
            $sal_change                 = $line_components[43];


            if (Product::where('product_id',$product_id)->exists()) {

                Product::where('product_id', $product_id)
                            ->update([
                                        'id_data_sheet'              => $id,
                                        'product_id'                 => $product_id,
                                        'name'                       => $name,
                                        'comercial_name'             => $comercial_name,
                                        'comercial_name_change'      => $comercial_name_change,
                                        'review_date'                => $review_date,
                                        'date'                       => $date,
                                        'package_type'               => $package_type,
                                        'package_type_change'        => $package_type_change,
                                        'conservation'               => $conservation,
                                        'conservation_change'        => $conservation_change,
                                        'lot_expiration'             => $lot_expiration,
                                        'lot_expiration_change'      => $lot_expiration_change,
                                        'caract_prod'                => $caract_prod,
                                        'caract_prod_change'         => $caract_prod_change,
                                        'ingredients'                => $ingredients,
                                        'ingredients_change'         => $ingredients_change,
                                        'ingredients_b'              => $ingredients_b,
                                        'ingredients_b_change'       => $ingredients_b_change,
                                        'ingredients_g'              => $ingredients_g,
                                        'ingredients_g_change'       => $ingredients_g_change,
                                        'valor_energetic'            => $valor_energetic,
                                        'valor_energetic_change'     => $valor_energetic_change,
                                        'proteines'                  => $proteines,
                                        'proteines_change'           => $proteines_change,
                                        'hidrats_carboni'            => $hidrats_carboni,
                                        'hidrats_carboni_change'     => $hidrats_carboni_change,
                                        'grasas'                     => $grasas,
                                        'grasas_change'              => $grasas_change,
                                        'caract_biolog'              => $caract_biolog,
                                        'caract_biolog_change'       => $caract_biolog_change,
                                        'us_esperat'                 => $us_esperat,
                                        'us_esperat_change'          => $us_esperat_change,
                                        'grup_consum'                => $grup_consum,
                                        'grup_consum_change'         => $grup_consum_change,
                                        'leaving_date'               => $leaving_date,
                                        'user'                       => $user,
                                        'grasas_saturadas'           => $grasas_saturadas,
                                        'grasas_saturadas_change'    => $grasas_saturadas_change,
                                        'hidrats_carb_azucar'        => $hidrats_carb_azucar,
                                        'hidrats_carb_azucar_change' => $hidrats_carb_azucar_change,
                                        'sal'                        => $sal,
                                        'sal_change'                 => $sal_change
                                    ]);
                
            }else{
               
                    Product::create([
                                        
                                        'id_data_sheet'              => $id,
                                        'product_id'                 => $product_id,
                                        'name'                       => $name,
                                        'comercial_name'             => $comercial_name,
                                        'comercial_name_change'      => $comercial_name_change,
                                        'review_date'                => $review_date,
                                        'date'                       => $date,
                                        'package_type'               => $package_type,
                                        'package_type_change'        => $package_type_change,
                                        'conservation'               => $conservation,
                                        'conservation_change'        => $conservation_change,
                                        'lot_expiration'             => $lot_expiration,
                                        'lot_expiration_change'      => $lot_expiration_change,
                                        'caract_prod'                => $caract_prod,
                                        'caract_prod_change'         => $caract_prod_change,
                                        'ingredients'                => $ingredients,
                                        'ingredients_change'         => $ingredients_change,
                                        'ingredients_b'              => $ingredients_b,
                                        'ingredients_b_change'       => $ingredients_b_change,
                                        'ingredients_g'              => $ingredients_g,
                                        'ingredients_g_change'       => $ingredients_g_change,
                                        'valor_energetic'            => $valor_energetic,
                                        'valor_energetic_change'     => $valor_energetic_change,
                                        'proteines'                  => $proteines,
                                        'proteines_change'           => $proteines_change,
                                        'hidrats_carboni'            => $hidrats_carboni,
                                        'hidrats_carboni_change'     => $hidrats_carboni_change,
                                        'grasas'                     => $grasas,
                                        'grasas_change'              => $grasas_change,
                                        'caract_biolog'              => $caract_biolog,
                                        'caract_biolog_change'       => $caract_biolog_change,
                                        'us_esperat'                 => $us_esperat,
                                        'us_esperat_change'          => $us_esperat_change,
                                        'grup_consum'                => $grup_consum,
                                        'grup_consum_change'         => $grup_consum_change,
                                        'leaving_date'               => $leaving_date,
                                        'user'                       => $user,
                                        'grasas_saturadas'           => $grasas_saturadas,
                                        'grasas_saturadas_change'    => $grasas_saturadas_change,
                                        'hidrats_carb_azucar'        => $hidrats_carb_azucar,
                                        'hidrats_carb_azucar_change' => $hidrats_carb_azucar_change,
                                        'sal'                        => $sal,
                                        'sal_change'                 => $sal_change  
                                    ]);

            }
         
        }

        $request->session()->flash('success', 'Fichas técnicas añadidas correctamente!');
        return redirect('data-sheet'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::where('id', $id)->first();

        return view('products.show', [ 'product' => $product ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $product = Product::where('id', $id)->first();

        $data = (array)$product['original'];

        dd($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::where('id', $id)->delete();
        return back();
    }
}
