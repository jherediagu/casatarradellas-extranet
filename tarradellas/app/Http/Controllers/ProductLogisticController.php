<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\ProductLogistic as ProductLogistic;

class ProductLogisticController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productLogistics = ProductLogistic::all();
        $total_logistics  = ProductLogistic::count();

        return view('products.logistics.index', [ 'logistics' => $productLogistics,
                                                    'total' => $total_logistics ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file       = $request->file('logistics');
        $file_name  = $request->file('logistics')->getClientOriginalName();
        $extension  = $file->guessExtension();

        if ($extension != 'txt') {
            
            $request->session()->flash('error', 'El archivo no es correcto, no es un archivo txt');
            return redirect('logistics');
        }

        foreach( file($file) as $line) {
            
            $line_components = explode('|', $line);

            $product_id         = $line_components[0];
            $name               = $line_components[1];
            $family_id          = $line_components[2];
            $ean_code           = $line_components[3];
            $dun_code           = $line_components[4];
            $dot_green          = $line_components[5];
            $pallet_box         = $line_components[6];
            $pallet_box_layer   = $line_components[7];
            $pallet_layer       = $line_components[8];
            $pallet_height      = $line_components[9];
            $box_tara           = $line_components[10]; 
            $box_units          = $line_components[11];
            $box_units_pack     = $line_components[12];
            $box_long           = $line_components[13];
            $box_width          = $line_components[14];
            $box_height         = $line_components[15];
            $box_net_weight     = $line_components[16];
            $box_gross_weight   = $line_components[17];
            $unit_long          = $line_components[18];
            $unit_width         = $line_components[19];
            $unit_height        = $line_components[20];
            $unit_diameter      = $line_components[21];
            $unit_net_weight    = $line_components[22];
            $unit_gross_weight  = $line_components[23];
            $expiration         = $line_components[24];


            if (ProductLogistic::where('product_id',$product_id)->exists()) {

                ProductLogistic::where('product_id', $product_id)
                            ->update([
                                        'product_id'        => $product_id,
                                        'name'              => $name,
                                        'family_id'         => $family_id,
                                        'ean_code'          => $ean_code,
                                        'dun_code'          => $dun_code,
                                        'dot_green'         => $dot_green,
                                        'pallet_box'        => $pallet_box,
                                        'pallet_box_layer'  => $pallet_box_layer,
                                        'pallet_layer'      => $pallet_layer,
                                        'pallet_height'     => $pallet_height,
                                        'box_tara'          => $box_tara,
                                        'box_units'         => $box_units,
                                        'box_units_pack'    => $box_units_pack,
                                        'box_long'          => $box_long,
                                        'box_width'         => $box_width,
                                        'box_height'        => $box_height,
                                        'box_net_weight'    => $box_net_weight,
                                        'box_gross_weight'  => $box_gross_weight,
                                        'unit_long'         => $unit_long,
                                        'unit_width'        => $unit_width,
                                        'unit_height'       => $unit_height,
                                        'unit_diameter'     => $unit_diameter,
                                        'unit_net_weight'   => $unit_net_weight,
                                        'unit_gross_weight' => $unit_gross_weight,
                                        'expiration'        => $expiration  
                                    ]);
                
            }else{
               
                ProductLogistic::create([
                                        'product_id'        => $product_id,
                                        'name'              => $name,
                                        'family_id'         => $family_id,
                                        'ean_code'          => $ean_code,
                                        'dun_code'          => $dun_code,
                                        'dot_green'         => $dot_green,
                                        'pallet_box'        => $pallet_box,
                                        'pallet_box_layer'  => $pallet_box_layer,
                                        'pallet_layer'      => $pallet_layer,
                                        'pallet_height'     => $pallet_height,
                                        'box_tara'          => $box_tara,
                                        'box_units'         => $box_units,
                                        'box_units_pack'    => $box_units_pack,
                                        'box_long'          => $box_long,
                                        'box_width'         => $box_width,
                                        'box_height'        => $box_height,
                                        'box_net_weight'    => $box_net_weight,
                                        'box_gross_weight'  => $box_gross_weight,
                                        'unit_long'         => $unit_long,
                                        'unit_width'        => $unit_width,
                                        'unit_height'       => $unit_height,
                                        'unit_diameter'     => $unit_diameter,
                                        'unit_net_weight'   => $unit_net_weight,
                                        'unit_gross_weight' => $unit_gross_weight,
                                        'expiration'        => $expiration  
                                    ]);

            }
         
        }

        $request->session()->flash('success', 'Fichas logísticas añadidas correctamente!');
        return redirect('logistics');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $request->session()->flash('error', 'No es posible editar las fichas logísticas');
        return redirect('logistics');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ProductLogistic::where('id', $id)->delete();
        return back();
    }
}
