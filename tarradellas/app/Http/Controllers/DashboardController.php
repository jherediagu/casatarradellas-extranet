<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\DeliveryNote as DeliveryNote;
use App\Product      as Product;

use DB;
use Auth;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
  

        if(Auth::user()->type == 2){

            $company_id = Auth::user()->company_id;

            $delivery_notes                 = DeliveryNote::select('*', DB::raw('count(*) as total'))
                                                                ->where('company_id', $company_id)
                                                                ->groupBy('delivery_note_id')
                                                                ->orderBy('created_at', 'desc')
                                                                ->take(5)->get();

            $delivery_notes_count           = DeliveryNote::where('validated', 1)
                                                                ->where('send', 1)
                                                                ->where('company_id', $company_id)
                                                                ->groupBy('delivery_note_id')
                                                                ->count();

            $delivery_notes_no_validated    = DeliveryNote::where('validated', 0)
                                                                ->where('company_id', $company_id)
                                                                ->where('send', 0)
                                                                ->groupBy('delivery_note_id')->count();

        }else{

            $delivery_notes                 = DeliveryNote::select('*', DB::raw('count(*) as total'))
                                                                ->orderBy('created_at', 'desc')
                                                                ->groupBy('delivery_note_id')
                                                                ->take(5)->get();
                                                                
            $delivery_notes_count           = DeliveryNote::where('validated', 1)->where('send', 1)->groupBy('delivery_note_id')->count();
            $delivery_notes_no_validated    = DeliveryNote::where('validated', 0)->where('send', 0)->groupBy('delivery_note_id')->count();
            $news = 0;
        }

        $products_total  = Product::count();

        return view('dashboard.index', [ 'delivery_notes'       => $delivery_notes , 
                                         'total_validated'      => $delivery_notes_count, 
                                         'total_no_validated'   => $delivery_notes_no_validated , 
                                         'products_total'       => $products_total  ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function contact()
    {
        return view('dashboard.contact');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function contactSend(Request $request)
    {

        //TODO falta servidor mail y gestion del email
        dd($request->input());

        $request->session()->flash('success', 'Contacto enviado correctamente!');
        return view('dashboard.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
