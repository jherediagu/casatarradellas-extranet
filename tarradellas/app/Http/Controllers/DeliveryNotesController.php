<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\DeliveryNoteFile as DeliveryNoteFile;
use App\DeliveryNote     as DeliveryNote;
use App\User             as User;
use App\CustomerDealer   as CustomerDealer;
use App\Product          as Product;
use App\Family           as Family;

use Auth;

class DeliveryNotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dealers =  User::where('type', 2)->get();
        $delivery_notes      = DeliveryNoteFile::all();
        $delivery_notes_total= DeliveryNoteFile::count();

        return view('delivery-notes.index', [ 'delivery_notes'   => $delivery_notes,
                                                      'total'    => $delivery_notes_total,
                                                      'dealers'  => $dealers  ]);
    }

    /**
     * Search a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $company_id     = $request->input('company_id');
        $import_date    = $request->input('import_date');
        $status         = $request->input('status');
        $created_at     = $request->input('created_at');


        // TODO Search
        dd($company_id ,$import_date ,$status,$created_at );

        return view('delivery-notes.index', [ 'delivery_notes'   => $delivery_notes,
                                                      'total'    => $delivery_notes_total,
                                                      'dealers'  => $dealers  ]);      
    }


    /**
     * Display a listing of the resource for dealer.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexDealer()
    {
        $company_id = Auth::user()->company_id;

        $delivery_notes      = DeliveryNoteFile::where('company_id', $company_id)->get();
        $delivery_notes_total= DeliveryNoteFile::where('company_id', $company_id)->count();

        return view('delivery-notes.index-dealer', [ 'delivery_notes'   => $delivery_notes,
                                                      'total'    => $delivery_notes_total  ]);
    }
    
    /**
     * Display a listing of the resource for manual entry.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexDealerManualEntry()
    {
        $id = Auth::user()->company_id;

        $delivery_notes      = DeliveryNote::where('dealer_id', $id)->groupBy('delivery_note_id')->get();
        $delivery_notes_total= DeliveryNote::where('dealer_id', $id)->groupBy('delivery_note_id')->count();

        return view('delivery-notes.index-manual', [ 'delivery_notes'   => $delivery_notes,
                                                      'total'    => $delivery_notes_total  ]);
    }


    /**
     * Display a listing of the resource for manual entry.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexDealerManualEntryCorrect(Request $request)
    {
        $id = Auth::user()->company_id;

        $delivery_notes      = DeliveryNote::where('dealer_id', $id)->groupBy('delivery_note_id')->get();
        $delivery_notes_total= DeliveryNote::where('dealer_id', $id)->groupBy('delivery_note_id')->count();
        
        $request->session()->flash('success', 'Albaran creado correctamente!');

        return view('delivery-notes.index-manual', [ 'delivery_notes'   => $delivery_notes,
                                                      'total'    => $delivery_notes_total  ]);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // dealer id
        $id = Auth::user()->nickname;
        $id = str_replace('REP', '', $id);

        $customers_dealers = CustomerDealer::where('dealer_id', $id)->get();

        // get all products and families
        $products = Product::all();
        $families = Family::all();

        return view("delivery-notes.create", [ 'customers_dealers' => $customers_dealers,
                                               'products'          => $products,
                                               'families'          => $families  ]);
    }




    /**
     * Store a newly created manual deliverynote resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeDeliveryNote(Request $request)
    {

        if($request->ajax()) {


            $last_entry = DeliveryNote::orderBy('created_at', 'desc')->max('delivery_note_id');
            $delivery_note_id = $last_entry+1;

            foreach ($request->all() as $products) {

                foreach ($products as $row) {
                   
                
                $date               = $row[0] != 0 ? date('Y-m-d',strtotime(str_replace('/', '-', $row[0]))) : '0';
                $order_date         = $row[1] != 0 ? date('Y-m-d',strtotime(str_replace('/', '-', $row[1]))) : '0';
                $customer_id        = $row[2];
                $order_id           = $row[3];
                $observations       = $row[4];
                $product_id         = $row[5];
                $boxes              = $row[7];
                $units              = $row[8];
                $lot                = $row[9];

                $company_id = Auth::user()->company_id;
                $user       = Auth::user()->nickname;

                $customer_dealer = CustomerDealer::where('customer_id', $customer_id)->first();
                
                $dealer_id = $customer_dealer->dealer_id;
                $carrier_id = $customer_dealer->carrier_id;

                if ($row[10] == 1) { $promotion = 'S'; }else{ $promotion = 'N'; }
                   
                DeliveryNote::create([
                                        'user'              => $user,
                                        'date'              => $date,
                                        'customer_id'       => $customer_id,
                                        'dealer_id'         => $dealer_id,
                                        'carrier_id'        => $carrier_id,
                                        'product_id'        => $product_id,
                                        'boxes'             => $boxes,
                                        'units'             => $units,
                                        'promotion'         => $promotion,
                                        'lot'               => $lot,
                                        'observations'      => $observations,
                                        'delivery_note_id'  => $delivery_note_id,
                                        'week'              => date("W").date('y'),
                                        'order_date'        => $order_date,
                                        'order_id'          => $order_id,
                                        'company_id'        => $company_id
                                      
                                    ]);
                
                }

            }
        

          
            return response()->json(['message' => "ok"]);
        }


        return response()->json(['message' => 'Error!']);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $id     = Auth::user()->company_id;
        $date   = date('Y-m-d H:i:s');

        $file       = $request->file('delivery-notes');
        $file_name  = $request->file('delivery-notes')->getClientOriginalName();
        $extension  = $file->guessExtension();

        // if extension != txt
        if ($extension != 'txt') {
            
            $request->session()->flash('error', 'El archivo no es correcto, no es un archivo txt');
            return redirect('delivery-notes');
        }

        // If exist the same file --> delete all existent row for update all.
        if (DeliveryNoteFile::where('file',$file_name)->exists()) {

            $delivery_note_file = DeliveryNoteFile::where('file', $file_name)->first();

            $id_deliver_note_file = $delivery_note_file->id;

            DeliveryNote::where('file_id', $delivery_note_file->id)->delete();

        }else{

            // else create new row with new file data
            $delivery_note_file = DeliveryNoteFile::create([  
                                                    'company_id'    => $id, 
                                                    'import_date'   => $date,
                                                    'file'          => $file_name ]);

            $id_deliver_note_file = $delivery_note_file->id;


        }

        // count rows of file
        $file_rows = 0;

        // new entry with every row
        foreach( file($file) as $line) {

            $file_rows++;
            
            $line_components = explode('|', $line);

            $user               = $line_components[1];
            $date               = $line_components[2] != 0 ? date('Y-m-d',strtotime(str_replace('/', '-', $line_components[2]))) : '0';
            $customer_id        = $line_components[3];
            $dealer_id          = $line_components[4];
            $carrier_id         = $line_components[5];
            $product_id         = $line_components[6];
            $boxes              = $line_components[7];
            $units              = $line_components[8];
            $promotion          = $line_components[10];
            $lot                = $line_components[11];
            $observations       = $line_components[12];
            $delivery_note_id   = $line_components[14];
            $week               = $line_components[15];
            $order_date         = $line_components[16] != 0 ? date('Y-m-d',strtotime(str_replace('/', '-', $line_components[16]))) : '0';
            $order_id           = $line_components[17];
               
            DeliveryNote::create([
                                    'user'              => $user,
                                    'date'              => $date,
                                    'customer_id'       => $customer_id,
                                    'dealer_id'         => $dealer_id,
                                    'carrier_id'        => $carrier_id,
                                    'product_id'        => $product_id,
                                    'boxes'             => $boxes,
                                    'units'             => $units,
                                    'promotion'         => $promotion,
                                    'lot'               => $lot,
                                    'observations'      => $observations,
                                    'delivery_note_id'  => $delivery_note_id,
                                    'week'              => $week,
                                    'order_date'        => $order_date,
                                    'order_id'          => $order_id,
                                    'company_id'        => $id,
                                    'file_id'           => $id_deliver_note_file
                                ]);
         
        }

        // update file info with total rows
        DeliveryNoteFile::where('id', $id_deliver_note_file)->update(['file_rows' => $file_rows]);

        // return to delivery notes section
        $request->session()->flash('success', 'Albaranes añadidos correctamente!');
        return redirect('delivery-notes');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function CheckValidate(Request $request,$id)
    {
        DeliveryNote::where('delivery_note_id', $id)->update([ 'validated' => 1 ]);

        $request->session()->flash('success', 'Albaran validado correctamente!');
        return redirect('delivery-notes-dealer-manual');

    }


    /**
     * Send delivery notes for admin.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function SendDeliveryNotes(Request $request)
    {




        $user_id    = Auth::user()->nickname;
        $company_id = Auth::user()->company_id;
        $date       = date('Y-m-d H:i:s');
        $file_name  = $user_id.date('YmdHi');
        
        $files_to_send = DeliveryNote::where('validated', 1)->where('send',0)->exists();

        if (!$files_to_send) {

            $request->session()->flash('error', 'No hay nigun archivo para enviar');
            return redirect('delivery-notes-dealer-manual');
        }

        $delivery_note_file = DeliveryNoteFile::create([  
                                                    'company_id'    => $company_id, 
                                                    'import_date'   => $date,
                                                    'file'          => $file_name ]);


        // update all as send
        DeliveryNote::where('validated', 1) ->where('send',0)
                                            ->update([  'send'      => 1 , 
                                                        'file_id'   => $delivery_note_id->id]);




        DeliveryNote::where('id', $id_deliver_note_file)->update(['file_id' => $delivery_note_id->id]);

        $request->session()->flash('success', 'Albaranes enviados correctamente!');
        return redirect('delivery-notes-dealer-manual');

    }


    /**
     * Download file TXT for delivery note.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function DownloadFile($id)
    {
        $file_rows  = DeliveryNote::where('file_id', $id)->get();
        $file_id    = DeliveryNoteFile::where('id', $id)->first();

        $text = "";
        foreach ($file_rows as $file) {

            $user               = $file['user'];
            $date               = $file['date'];
            $customer_id        = $file['customer_id'];
            $dealer_id          = $file['dealer_id'];
            $carrier_id         = $file['carrier_id'];
            $product_id         = $file['product_id'];
            $boxes              = $file['boxes'];
            $units              = $file['units'];
            $promotion          = $file['promotion'];
            $lot                = $file['lot'];
            $observations       = $file['observations'];
            $delivery_note_id   = $file['delivery_note_id'];
            $week               = $file['week'];
            $order_date         = $file['order_date'];
            $order_id           = $file['order_id']; 


            $text.= "0|".$user."|".$date."|".$customer_id."|".$dealer_id."|".$carrier_id."|".$product_id."|".$boxes."|".$units."|".$promotion."|".$lot."|".$observations."|A|".$delivery_note_id."|".$week."|".$order_date."|".$order_id."|\n";
        }

        $file_name = $file_id['file'];
        $headers = ['Content-type'=>'text/plain', 'Content-Disposition'=>sprintf('attachment; filename="%s"', $file_name),'Content-Length'=>strlen($text)];
        
        return response($text, 200, $headers);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete row and rows in delivery notes
        DeliveryNote::where('file_id', $id)->delete();
        DeliveryNoteFile::where('id', $id)->delete();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyDeliveryNoteLines($id)
    {
        // delete row and rows in delivery notes
        DeliveryNote::where('delivery_note_id', $id)->delete();
        return back();
    }
}
