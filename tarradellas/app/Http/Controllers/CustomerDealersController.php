<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\CustomerDealer as CustomerDealer;

class CustomerDealersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers_dealers      = CustomerDealer::all();
        $customers_dealers_total= CustomerDealer::count();

        return view('customers-dealers.index', [ 'customers_dealers'   => $customers_dealers,
                                                            'total'    => $customers_dealers_total ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file       = $request->file('customers_dealers');
        $file_name  = $request->file('customers_dealers')->getClientOriginalName();
        $extension  = $file->guessExtension();

        if ($extension != 'txt') {
            
            $request->session()->flash('error', 'El archivo no es correcto, no es un archivo txt');
            return redirect('customers-dealers');
        }

        foreach( file($file) as $line) {
            
            $line_components = explode('|', $line);

            $dealer_id          = $line_components[0];
            $customer_id        = $line_components[1];
            $name               = $line_components[2];
            $comercial_name     = $line_components[3];
            $address            = $line_components[4];
            $postal_code        = $line_components[5];
            $city               = $line_components[6];
            $province           = $line_components[7];
            $NIF                = $line_components[8];
            $carrier_id         = $line_components[9];
            $carrier_address    = $line_components[10]; 
            $carrier_postal_code= $line_components[11];
            $carrier_city       = $line_components[12];
            $carrier_province   = $line_components[13];
            $carrier_name       = $line_components[14];


            if (CustomerDealer::where('dealer_id',$dealer_id)->exists()) {

                CustomerDealer::where('dealer_id', $dealer_id)
                            ->update([
                                        'dealer_id'           => $dealer_id,
                                        'customer_id'         => $customer_id,
                                        'name'                => $name,
                                        'comercial_name'      => $comercial_name,
                                        'address'             => $address,
                                        'postal_code'         => $postal_code,
                                        'city'                => $city,
                                        'province'            => $province,
                                        'NIF'                 => $NIF,
                                        'carrier_id'          => $carrier_id,
                                        'carrier_address'     => $carrier_address,
                                        'carrier_postal_code' => $carrier_postal_code,
                                        'carrier_city'        => $carrier_city,
                                        'carrier_province'    => $carrier_province,
                                        'carrier_name'        => $carrier_name
                                    ]);
                
            }else{
               
                CustomerDealer::create([
                                        'dealer_id'           => $dealer_id,
                                        'customer_id'         => $customer_id,
                                        'name'                => $name,
                                        'comercial_name'      => $comercial_name,
                                        'address'             => $address,
                                        'postal_code'         => $postal_code,
                                        'city'                => $city,
                                        'province'            => $province,
                                        'NIF'                 => $NIF,
                                        'carrier_id'          => $carrier_id,
                                        'carrier_address'     => $carrier_address,
                                        'carrier_postal_code' => $carrier_postal_code,
                                        'carrier_city'        => $carrier_city,
                                        'carrier_province'    => $carrier_province,
                                        'carrier_name'        => $carrier_name
                                    ]);

            }
         
        }

        $request->session()->flash('success', 'Clientes distribuidores añadidos correctamente!');
        return redirect('customers-dealers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
