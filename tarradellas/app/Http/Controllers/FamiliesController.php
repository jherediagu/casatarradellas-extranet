<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Family as Family;

use File;

class FamiliesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $families       = Family::all();
        $total_families = Family::count();


        return view('products.families.index', [ 'families' => $families,
                                                    'total' => $total_families ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $file       = $request->file('families');
        $file_name  = $request->file('families')->getClientOriginalName();
        $extension  = $file->guessExtension();

        if ($extension != 'txt') {
            
            $request->session()->flash('error', 'El archivo no es correcto, no es un archivo txt');
            return redirect('families');
        }

        foreach( file($file) as $line) {
            
            $line_components = explode('|', $line);

            $family_id   = $line_components[0];
            $family_name = $line_components[1];


            if (Family::where('family_id',$family_id)->exists()) {

                Family::where('family_id', $family_id)
                            ->update(['family_id' => $family_id, 'name' => $family_name]);
                
            }else{
               
                Family::create(['family_id' => $family_id , 'name' => $family_name]);
            }
         
        }

        $request->session()->flash('success', 'Familias añadidas correctamente!');
        return redirect('families');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $request->session()->flash('error', 'No es posible editar las familias');
        return redirect('families');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Family::where('id', $id)->delete();
        return back();
    }
}
