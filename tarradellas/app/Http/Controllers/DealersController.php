<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Dealer as Dealer;


class DealersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dealers = Dealer::where('type' , 2)->get();
        $dealers_total = Dealer::where('type' , 2)->count();

        return view('dealers.index', [ 'dealers' => $dealers, 
                                         'total' => $dealers_total]);
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dealers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $dealer_data = $request->except('password');

        $dealer = new Dealer($dealer_data);

        $dealer->password = bcrypt($request->input('password'));

        $dealer->save();

        $request->session()->flash('success', 'Distribuidor creado correctamente!');
        return redirect('dealers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dealer = Dealer::where('id', $id)->first();

        return view('dealers.edit',['dealer' => $dealer]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $dealer = Dealer::find($id);
        
        $dealer_data = $request->except('password');

        if ($request->has('password'))
        {
            $dealer->password = bcrypt($request->input('password'));
        }

        $dealer->fill($dealer_data);

        $dealer->save();

        $request->session()->flash('success', 'Distribuidor editado correctamente!');
        return redirect('dealers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete row
        Dealer::where('id', $id)->delete();
        return back();
    }
}
