<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



  Route::get('/', function () {
    return view('welcome');
});




  Route::get('/aaa', function () {
    return "hola!";
});



// Authentication routes...
Route::post('auth/login', 'Auth\AuthController@postLogin');			// Post login
Route::get('auth/logout', 'Auth\AuthController@getLogout');			// Logout

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');		// Registration
Route::post('auth/register', 'Auth\AuthController@postRegister');	// Post Register


Route::group(['middleware' => 'auth'], function () {


	// Dashboard
	Route::get('dashboard', 'DashboardController@index');  	// Dashboard index

	// Contact
	Route::get('contact', 'DashboardController@contact');  			// Contact
	Route::post('contact/send', 'DashboardController@contactSend'); // Contact Send


	/*
	|
	| User type 2 ( Dealer )
	|
	*/
	Route::get('notice-dealer', 'NewsController@indexDealer');  // Index News Dealer
	Route::get('notice-dealer/{id}', 'NewsController@showNewsDealer');   // Show News Dealer

	
	// Products for all users ( Admin, Dealer, Client )
	Route::resource('products', 'ProductsController'); 	// Resources Products --> GET / POST / PUT / DELETE
	
	// Delivery notes Dealer index
	Route::get('delivery-notes-dealer', 'DeliveryNotesController@indexDealer');

	// Delivery notes Dealer manual
	Route::get('delivery-notes-dealer-manual', 				'DeliveryNotesController@indexDealerManualEntry');	// Index manual entry delivery notes
	Route::get('delivery-notes-dealer-manual/correct', 		'DeliveryNotesController@indexDealerManualEntryCorrect');	// Index manual entry delivery notes Created!

	Route::get('delivery-notes-dealer-manual/check/{id}', 	'DeliveryNotesController@CheckValidate');			// validate delivery note
	Route::get('delivery-notes-dealer-manual/send', 		'DeliveryNotesController@SendDeliveryNotes');		// send all validated delivery notes
	Route::get('delivery-notes-dealer-manual/create', 		'DeliveryNotesController@create');		// Create delivery note
	Route::get('delivery-notes-dealer-manual/delete/{id}', 	'DeliveryNotesController@destroyDeliveryNoteLines');		// Create delivery note


	Route::resource('delivery-notes', 					'DeliveryNotesController');  					// Resources Delivery note --> GET / POST / PUT / DELETE
	Route::get('delivery-notes/delete/{id}',		 	'DeliveryNotesController@destroy'); 			// Dealers --> DELETE
	Route::post('delivery-notes/search', 				'DeliveryNotesController@search'); 				// Dealers --> Search
	Route::post('delivery-notes/create-delivery-note', 	'DeliveryNotesController@storeDeliveryNote'); 	// Dealers --> store delivery note

	Route::get('delivery-notes/file/{id}', 'DeliveryNotesController@DownloadFile'); // Dealers --> Download File TXT Delivery note 


	/*
	|--------------------------------------------------------------------------
	| Middleware -> Identify user
	|--------------------------------------------------------------------------
	|
	| User type 0 ( Admin )
	|
	*/
	Route::group(['middleware' => 'App\Http\Middleware\UserAdmin'], function()
	{

		// Dealers
		Route::resource('dealers', 'DealersController'); 				// Resources Dealers --> GET / POST / PUT / DELETE
		Route::get('dealers/delete/{id}', 'DealersController@destroy'); // Dealers --> DELETE


		// Customers
		Route::resource('customers', 'CustomersController'); 	// Resources Customers --> GET / POST / PUT / DELETE
		Route::get('customers/delete/{id}', 'CustomersController@destroy'); // Customers --> DELETE


		// News
		Route::resource('news', 'NewsController'); 	// Resources News --> GET / POST / PUT / DELETE
		Route::get('news/delete/{id}', 'NewsController@destroy'); // News --> DELETE


		// Products
		Route::get('products/delete/{id}', 'ProductsController@destroy'); // Products --> DELETE


		// ProductsLogistics
		Route::resource('logistics', 'ProductLogisticController'); 	// Resources Products Logistics --> GET / POST / PUT / DELETE
		Route::get('logistics/delete/{id}', 'ProductLogisticController@destroy'); // Products Logistics --> DELETE


		// Product data sheet
		Route::get('data-sheet', 'ProductsController@indexDataSheet'); // Data sheet list
		Route::get('data-sheet/delete/{id}', 'ProductsController@destroy'); // Data sheet list


		// Families
		Route::resource('families', 'FamiliesController'); 	// Resources Families --> GET / POST / PUT / DELETE
		Route::get('families/delete/{id}', 'FamiliesController@destroy'); // Products Families --> DELETE


		// Customers dealers
		Route::resource('customers-dealers', 'CustomerDealersController'); 	// Resources Customers Dealers --> GET / POST / PUT / DELETE
		Route::get('customers-dealers/delete/{id}', 'CustomerDealersController@destroy'); // Products Customers Dealers --> DELETE


	});





});