<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dealer extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'nickname', 'created_at', 'updated_at', 'type'];


}
