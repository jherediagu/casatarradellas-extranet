<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Family extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_families';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'family_id', 'name', 'created_at', 'updated_at'];


    public function logistic()
    {
        return $this->BelongsTo('App\ProductLogistic', 'family_id', 'family_id');
    }

}
