<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableForProductsWithDataAndLogis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {

            // Products
            $table->increments('id');

            $table->integer('id_data_sheet');

            $table->integer('product_id');
            $table->string('name');

            $table->string('comercial_name');
            $table->tinyInteger('comercial_name_change');

            $table->date('review_date');
            $table->date('date');

            $table->text('package_type');
            $table->tinyInteger('package_type_change');

            $table->text('conservation');
            $table->tinyInteger('conservation_change');

            $table->text('lot_expiration');
            $table->tinyInteger('lot_expiration_change');

            $table->text('caract_prod');
            $table->tinyInteger('caract_prod_change');

            $table->text('ingredients');
            $table->tinyInteger('ingredients_change');

            $table->text('ingredients_b');
            $table->tinyInteger('ingredients_b_change');

            $table->text('ingredients_g');
            $table->tinyInteger('ingredients_g_change');
            
            $table->text('valor_energetic');
            $table->tinyInteger('valor_energetic_change');

            $table->text('proteines');
            $table->tinyInteger('proteines_change');

            $table->text('hidrats_carboni');
            $table->tinyInteger('hidrats_carboni_change');

            $table->text('grasas');
            $table->tinyInteger('grasas_change');

            $table->text('caract_biolog');
            $table->tinyInteger('caract_biolog_change');

            $table->text('us_esperat');
            $table->tinyInteger('us_esperat_change');

            $table->text('grup_consum');
            $table->tinyInteger('grup_consum_change');

            $table->date('leaving_date');
            $table->string('user');

            $table->string('grasas_saturadas');
            $table->tinyInteger('grasas_saturadas_change');

            $table->string('hidrats_carb_azucar');
            $table->tinyInteger('hidrats_carb_azucar_change');

            $table->string('sal');
            $table->tinyInteger('sal_change');

            // Timestamps ( created_at / updated_at )
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
