<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLogisticForRelationWithProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_logistic_data', function (Blueprint $table) {

            // logistic data
            $table->increments('id');
            $table->integer('product_id');
            $table->string('name');
            $table->integer('family_id');

            // 13
            $table->bigInteger('ean_code');
            
            // 14
            $table->bigInteger('dun_code');

            // ??
            $table->decimal('dot_green');

            // Pallet data
            $table->integer('pallet_box');
            $table->integer('pallet_box_layer');
            $table->integer('pallet_layer');
            $table->decimal('pallet_height');

            // box data
            $table->decimal('box_tara');
            $table->integer('box_units');
            $table->integer('box_units_pack');

            // box data size
            $table->decimal('box_long');
            $table->decimal('box_width');
            $table->decimal('box_height');

            // box weight
            $table->decimal('box_net_weight');
            $table->decimal('box_gross_weight');


            // Units data
            $table->decimal('unit_long');
            $table->decimal('unit_width');
            $table->decimal('unit_height');
            $table->decimal('unit_diameter');

            // Units weight
            $table->decimal('unit_net_weight');
            $table->decimal('unit_gross_weight');

            // Expiration
            $table->integer('expiration');

            // Timestamps ( created_at / updated_at )
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products_logistic_data');
    }
}
