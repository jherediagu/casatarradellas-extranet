<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableForDeliveryNoteFilesRelation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery-notes-files', function (Blueprint $table) {

            // Delivery notes files 
            $table->increments('id');

            $table->integer('company_id'); // company_id --> table users
            $table->datetime('import_date');
            $table->string('file');

            $table->integer('file_rows');

            // status == 0 -> pendiente
            // status == 1 -> procesado
            // status == 2 -> incorrecto
            $table->integer('status');

            // Timestamps ( created_at / updated_at )
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('delivery-notes-files');
    }
}
