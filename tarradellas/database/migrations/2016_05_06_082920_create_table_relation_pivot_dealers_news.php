<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRelationPivotDealersNews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relation_news_dealers', function (Blueprint $table) {

            // News
            $table->increments('id');
            $table->integer('id_news');
            $table->integer('id_dealer');
            $table->integer('viewed');

            // Timestamps ( created_at / updated_at )
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('relation_news_dealers');
    }
}
