<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableForDeliveryNotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery-notes', function (Blueprint $table) {

            // Delivery notes
            $table->increments('id');

            $table->string('user');
            $table->date('date');

            $table->integer('customer_id');
            $table->integer('dealer_id');
            $table->integer('carrier_id');
            $table->integer('product_id');
            $table->integer('boxes');
            $table->integer('units');

            $table->string('promotion');
            $table->integer('lot');
            $table->string('observations');
            
            $table->integer('delivery_note_id');
            $table->integer('week');
            $table->date('order_date');
            $table->string('order_id');

            $table->integer('validated');   // 0 -> no / 1 -> yes
            $table->integer('send');        // 0 -> no / 1 -> yes

            $table->integer('company_id'); 
            
            // file id
            $table->integer('file_id'); 


            // Timestamps ( created_at / updated_at )
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('delivery-notes');
    }
}
