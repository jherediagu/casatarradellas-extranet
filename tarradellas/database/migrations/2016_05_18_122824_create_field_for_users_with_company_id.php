<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldForUsersWithCompanyId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($tabla) 
        {
            // create column company_id for imported files txt
            $tabla->integer('company_id');

        });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($tabla) 
        {
            // drop column company_id
            $tabla->dropColumn('company_id');

        });
    }
}
