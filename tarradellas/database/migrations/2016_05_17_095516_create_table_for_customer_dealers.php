<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableForCustomerDealers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer-dealers', function (Blueprint $table) {

            // logistic data
            $table->increments('id');
            $table->integer('dealer_id');
            $table->integer('customer_id');
           
            $table->string('name');
            $table->string('comercial_name');
            $table->string('address');

            $table->integer('postal_code');
            $table->string('city');
            $table->string('province');
            $table->string('NIF');

            $table->integer('carrier_id');

            $table->string('carrier_address');
            $table->string('carrier_postal_code');
            $table->string('carrier_city');
            $table->string('carrier_province');
            $table->string('carrier_name');

            // Timestamps ( created_at / updated_at )
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customer-dealers');
    }
}
