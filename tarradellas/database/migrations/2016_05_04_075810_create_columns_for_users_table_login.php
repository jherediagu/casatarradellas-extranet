<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColumnsForUsersTableLogin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('users', function($tabla) 
        {
            // create column image
            $tabla->string('nickname')->unique();


             /**
             * User Types.
             *
             * Admin                = 0
             * Casa tarradellas     = 1 ( now -> 0 / same as admin )
             * Distribuidor/Dealer  = 2
             * Client/Customer      = 3
             *
             */
            $tabla->integer('type');

        });    

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($tabla) 
        {
            // drop column image
            $tabla->dropColumn('nickname');
            $tabla->dropColumn('type');

        });
    }
}
