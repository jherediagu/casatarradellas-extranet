@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')


        <section id="content" style="padding-top: 50px; z-index: 3">
                <div class="container">
                    <div class="block-header">
                        <h2>Albaranes</h2>
                        
                    </div>
                    

    <div class="row">
        <div class="col-md-8">
            
            <div class="card">
                <div class="card-header">
                    <h2>Añadir Albaranes<small>Importar albaranes a partir de un archivo .txt</small></h2>
                </div>
                        
                <div class="card-body card-padding">
                    <form method="POST" action="{{URL::to('/')}}/delivery-notes" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <span class="btn btn-primary btn-file bgm-gray1">
                                    <span class="fileinput-new">Seleccionar archivo para enviar</span>
                                    <span class="fileinput-exists">Canviar el archivo cargado</span>
                                    <input type="file" name="delivery-notes">
                                </span>
                                <span class="fileinput-filename"></span>
                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <button type="submit" class="btn btn-primary btn-md bgm-gray1">Enviar</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>

        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h2>Entrada manual<small>Ir al listado de albaranes validados y la entrada manual.</small></h2>
                </div>
                        
                <div class="card-body card-padding" style="padding-bottom: 31px">                
                    <a href="{{URL::to('/')}}/delivery-notes-dealer-manual">
                        <button type="submit" class="btn btn-primary btn-md bgm-gray1">Entrada manual</button>
                    </a>
                </div>
            </div>
        </div>
    </div>

            <div class="card" style=" z-index: 1">

                        <div class="card-header">
                            <div class="row">
                                <div class="col-sm-6 col-md-6">
                                    <h2>Listado de Albaranes <small>Lista completa de los albaranes disponibles.</small></h2>
                                </div>
                                <div class="col-sm-6 col-md-4 col-md-offset-2">
                                    <div class="mini-charts-item bgm-bluegray" style=" margin:0px;margin-top: 25px">
                                        <div class="clearfix">
                                            <div class="chart stats-line-2"></div>
                                            <div class="count">
                                                <small>Total actual de albaranes</small>
                                                <h2>{!! $total !!}</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    

                    @if($delivery_notes->isEmpty())



                    <div class="container" style="text-align: center; padding-top: 60px; padding-bottom: 60px">

                        <h3>Actualmente no hay nigun albaran registrado.</h3>
                    </div>

                    @else
                            
                        <table id="data-table-dealer-delivery" class="table table-striped table-vmiddle">
                            <thead>
                                <tr>
                                    <th data-column-id="id" data-type="numeric" data-width="7%">ID</th>
                                    <th data-column-id="dealer_id" data-width="20%">Fecha importación</th>
                                    <th data-column-id="customer_id" data-width="30%">Archivo</th>
                                    <th data-column-id="name" data-width="13%">Nº de registros</th>
                                    <th data-column-id="nif" data-width="12%">Estado</th>
                                    <th data-column-id="date" data-order="desc">Fecha creación</th>
                                </tr>
                            </thead>
                            <tbody>

                            @foreach( $delivery_notes as $notes )
                                <tr>
                                    <td>{!! $notes->id !!}</td>
                                    <td>{!! $notes->import_date !!}</td>
                                    <td><a href="{!! $notes->file !!}">{!! $notes->file !!}</a></td>
                                    <td>{!! $notes->file_rows !!}</td>
                                    @if ( $notes->status == 0)
                                        <td>Pendiente</td>
                                    @elseif( $notes->status == 1)
                                        <td>Procesado</td>
                                    @else
                                        <td>Incorrecto</td>
                                    @endif
                                    <td>{!! $notes->created_at !!}</td>
                                </tr>
                            @endforeach
                                                 
                            </tbody>
                        </table>
                    @endif
                    </div>

            </div>
    </section>


@include('dashboard.layouts.footer')