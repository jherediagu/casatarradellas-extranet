@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')


        <section id="content" style="padding-top: 50px; z-index: 3">
                <div class="container">
                    <div class="block-header">
                        <h2>Albaranes</h2>
                        
                    </div>
                    


    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h2>Nuevo Albarán<small>Crear albarán de forma manual.</small></h2>
                </div>
                        
                <div class="card-body card-padding" style="padding-bottom: 31px">                
                    <a href="{{URL::to('/')}}/delivery-notes-dealer-manual/create">
                        <button type="submit" class="btn btn-primary btn-md bgm-gray1">Crear Albarán</button>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h2>Enviar Validados<small>Enviar todos los albaranes validados.</small></h2>
                </div>
                        
                <div class="card-body card-padding" style="padding-bottom: 31px">                
                    <a href="{{URL::to('/')}}/delivery-notes-dealer-manual/send">
                        <button type="submit" class="btn btn-primary btn-md bgm-gray1">Enviar validados</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
                    
            <div class="card" style=" z-index: 1">

                        <div class="card-header">
                            <div class="row">
                                <div class="col-sm-6 col-md-6">
                                    <h2>Listado de Albaranes <small>Lista completa de los albaranes disponibles.</small></h2>
                                </div>
                                <div class="col-sm-6 col-md-4 col-md-offset-2">
                                    <div class="mini-charts-item bgm-bluegray" style=" margin:0px;margin-top: 25px">
                                        <div class="clearfix">
                                            <div class="chart stats-line-2"></div>
                                            <div class="count">
                                                <small>Total actual de albaranes</small>
                                                <h2>{!! $total !!}</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    

                    @if($delivery_notes->isEmpty())



                    <div class="container" style="text-align: center; padding-top: 60px; padding-bottom: 60px">

                        <h3>Actualmente no hay nigun albaran registrado.</h3>
                    </div>

                    @else
                            
                        <table id="data-table-delivery-note" class="table table-striped table-vmiddle">
                            <thead>
                                <tr>
                                    <th data-column-id="id" data-type="numeric" data-width="7%">ID</th>
                                    <th data-column-id="deliverynotesid" data-width="12%">Num. Albarán</th>
                                    <th data-column-id="date" data-order="desc" data-width="12%">Fecha</th>
                                    <th data-column-id="customer" data-width="30%">Cliente</th>
                                    <th data-column-id="validated" data-width="9%">Validado</th>
                                    <th data-column-id="send" data-width="9%">Enviado</th>
                                    <th data-column-id="commands" data-formatter="commands" data-sortable="false">Opciones</th>
                                </tr>
                            </thead>
                            <tbody>

                            @foreach( $delivery_notes as $notes )
                                <tr>
                                    <td>{!! $notes->id !!}</td>
                                    <td>{!! $notes->delivery_note_id !!}</td>
                                    <td>{!! $notes->created_at !!}</td>
                                    <td>{!! $notes->customers['name'] !!}</td>
                                    <td class="center">{!! $notes->validated == 0 ? "No" : "Si" !!}</td>
                                    <td>{!! $notes->send == 0 ? "No" : "Si" !!}</td>
                                </tr>
                            @endforeach
                                                  
                            </tbody>
                        </table>
                    @endif
                    </div>

            </div>
    </section>


@include('dashboard.layouts.footer')
