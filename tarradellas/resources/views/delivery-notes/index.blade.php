@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')


        <section id="content" style="padding-top: 50px; z-index: 3">
                <div class="container">
                    <div class="block-header">
                        <h2>Albaranes</h2>
                        
                    </div>
                    


                    <div class="card">
                        <div class="card-header">
                            <h2>Buscar albaranes</h2>
                        </div>
                        
                        <div class="card-body card-padding">
                            <form method="POST" action="{{URL::to('/')}}/delivery-notes/search" enctype="multipart/form-data" class="row" role="form">
                                {!! csrf_field() !!}
                          
                            <div class="col-sm-4">
                                <div class="form-group fg-line">
                                    <label>Referencia distribuidor</label><br>
                                        <select name="company_id" class="select2" style="width: 100%">
                                                <option value="0"></option>
                                            @foreach ($dealers as $dealer)
                                                <option value="{!! $dealer->company_id !!}">{{$dealer->name}}</option>
                                            @endforeach
                                        </select>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group fg-line">
                                    <label>Estado</label><br>
                                        <select name="status" class="select2" style="width: 100%">
                                            <option value="0"></option>
                                            <option value="1">INCORRECTO</option>
                                            <option value="2">PENDIENTE</option>
                                            <option value="3">PROCESADO</option>
                                        </select>
                                </div>
                            </div> 

                                 <div class="col-sm-2">
                                    <div class="form-group ">
                                    <label>Desde:</label>
                                        <input name="import_date" type="text" class="form-control input-mask" data-mask="00/00/0000" placeholder="ejemplo: 23/05/2014">
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group ">
                                    <label>Hasta:</label>
                                        <input name="created_at" type="text" class="form-control input-mask" data-mask="00/00/0000" placeholder="ejemplo: 23/05/2014">
                                    </div>
                                </div>
                            
                                
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-primary btn-md bgm-gray" style="margin-top:20px">buscar</button>
                                </div>
                            </form>
                        </div>
                    </div>

            <div class="card" style=" z-index: 1">

                        <div class="card-header">
                            <div class="row">
                                <div class="col-sm-6 col-md-6">
                                    <h2>Listado de Albaranes <small>Lista completa de los albaranes disponibles.</small></h2>
                                </div>
                                <div class="col-sm-6 col-md-4 col-md-offset-2">
                                    <div class="mini-charts-item bgm-bluegray" style=" margin:0px;margin-top: 25px">
                                        <div class="clearfix">
                                            <div class="chart stats-line-2"></div>
                                            <div class="count">
                                                <small>Total actual de albaranes</small>
                                                <h2>{!! $total !!}</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    

                    @if($delivery_notes->isEmpty())



                    <div class="container" style="text-align: center; padding-top: 60px; padding-bottom: 60px">

                        <h3>Actualmente no hay nigun albaran registrado.</h3>
                    </div>

                    @else
                            
                        <table id="data-table-delivery" class="table table-striped table-vmiddle">
                            <thead>
                                <tr>
                                    <th data-column-id="id" data-type="numeric" data-width="7%">ID</th>
                                    <th data-column-id="company_id">Distribuidor</th>
                                    <th data-column-id="customer_id" data-width="20%">Archivo</th>
                                    <th data-column-id="created_at" data-width="12%">Fecha</th>
                                    <th data-column-id="name" data-width="10%">Nº de registros</th>
                                    <th data-column-id="nif" data-width="12%">Estado</th>
                                    <th data-column-id="commands" data-formatter="commands" data-sortable="false">Opciones</th>

                                </tr>
                            </thead>
                            <tbody>

                            @foreach( $delivery_notes as $notes )
                                <tr>
                                    <td>{!! $notes->id !!}</td>
                                    <td>{!! $notes->users->name !!}</td>
                                    <td>{!! $notes->file !!}</td>
                                    <td>{!! $notes->created_at !!}</td>
                                    <td>{!! $notes->file_rows !!}</td>
                                    @if ( $notes->status == 0)
                                        <td>Pendiente</td>
                                    @elseif( $notes->status == 1)
                                        <td>Procesado</td>
                                    @else
                                        <td>Incorrecto</td>
                                    @endif
                                </tr>
                            @endforeach
                                                 
                            </tbody>
                        </table>
                    @endif
                    </div>

            </div>
    </section>


@include('dashboard.layouts.footer')
