@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')


<style type="text/css">
	
	input {
	    width: 100%;
	    padding: 4px 10px;
	   	border-radius: 5px;
 		border:1.2px solid #AAAAAA; 
	}
	select {
	    width: 100%;
	    padding: 4px 10px;
	   	border-radius: 5px;
 		border:1.2px solid #AAAAAA; 
 		   height: 28px;

	}
</style>

<section id="content" style="padding-top: 50px; z-index: 3">
	<div class="container">
		<div class="block-header">
			<h2>Crear albaran</h2>
		</div>
                    
    <div class="row">
        <div class="col-md-12">
            
            <div class="card">
                <div class="card-header">
                    <h2>Crear nuevo albarán</h2>
                </div>
                 
                <form  id="formId" method="POST" action="{{URL::to('/')}}/delivery-notes/create-delivery-note">
              	{!! csrf_field() !!}

                <div class="card-body card-padding">
                    <div class="row">
                    	<div class="col-md-5">
                        	<div class="form-group ">
                            	<label>Fecha del Albarán:</label>
                                <input name="deliverynote_date" type="text" class="form-control input-mask" data-mask="00/00/0000" placeholder="ejemplo: 23/05/2016">
                            </div>
                        </div>
                    	<div class="col-md-5">
                        	<div class="form-group ">
                            	<label>Fecha del pedido:</label>
                                <input name="order_date" type="text" class="form-control input-mask" data-mask="00/00/0000" placeholder="ejemplo: 23/05/2016">
                            </div>
                        </div>
                      	<div class="col-md-7">
                        	<div class="form-group fg-line">
                        	    <label>Referencia distribuidor</label><br>
                        	    <select id="select_customer" name="select_customer" class="select2" style="width: 100%">
                        	    	<option value="0"></option>
                        	        @foreach ($customers_dealers as $customers)
	                                	<option value="{!! $customers->customer_id !!}">
	                                		{{$customers->name}} | {{$customers->address}} | {{$customers->postal_code}} | {{$customers->city}}, {{$customers->province}} | {{$customers->NIF}}
	                     	            </option>
                         	        @endforeach
                         	     </select>
                         	</div>
                        </div>
                        <div class="col-md-3">
                        	<div class="form-group ">
                            	<label>Numero del pedido:</label>
                                <input name="order_number" type="number" class="form-control" placeholder="pedido">
                            </div>
                        </div>

                        <div class="col-md-10">
    						<div class="form-group">
								<label>Observaciones</label><br>
                                <div class="fg-line">
                                    <textarea name="observations" class="form-control" rows="5" placeholder="Escribe sus observacions aquí."></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>



    <div class="row">
        <div class="col-md-12">
            
            <div class="card">
                <div class="card-header">
                    <h2>Productos</h2><small>seleccione los productos que quiere añadir del desplegable.</small>
                </div>

                <div class="card-body card-padding">
                	<div class="row">
						<div class="col-sm-8">
                 			<div class="form-group fg-line">
                            	<select id="select-product" name="select_product" class="select2products" style="width: 100%">
                                	<option value="0">Selecciona el producto de la lista</option>
                                    @foreach ($families as $family)
	                                    <optgroup label="{!! $family->name !!}">
	                                    	@foreach($products as $product)
                                            	@if($product->logistics['family_id'] == $family->family_id)
	                                            	<option value="{!! $product->id !!}" 
	                                                		idproduct="{!! $product->product_id !!}"
															nameproduct="{!! $product->name !!}"
															boxunits="{!! $product->logistics->box_units !!}"
	                                                		>
	                                                {{$product->product_id}} - {{$product->name}}
	                                                </option>
	                                            @endif
											@endforeach
										</optgroup>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-sm-4">
                        	<a id="add-product" class="btn btn-primary btn-sm bgm-gray">Añadir</a>
                        </div>
                    </div>
                </div>
                        
  				<div class="table-responsive" style="padding-bottom: 20px">
                	<table class="table table-striped" id="table-products">
                    	<thead>
                     		<tr>
                     	    	<th width="5%">Codigo</th>
                     	    	<th>Producto</th>
                     	    	<th width="10%">Cajas</th>
                     	    	<th width="11%">Unidades</th>
                     	    	<th width="10%">Lote</th>
								<th width="5%">Género s/cargo</th>
<!-- 								<th width="8%">Devolución</th>
 -->								<th width="5%"></th>
                     	    </tr>
                     	</thead>
                     	<tbody>
                     	<tr>
    <!--                                 <td><input name="product_id[]" type="hidden" ></td>
                                    <td><input name="product_name[]" type="hidden"></td>
                                    <td><input class="boxes" name="boxes[]" type="number" placeholder="cajas"></td>
                                    <td><input name="units[]" placeholder="unidades" readonly></td>
                                    <td><input name="lot[]" type="number"  placeholder="lote" required></td>
                                    <td>                             
                                       <select name="company_id[]" class="select2" >
                                            <option value="0">No</option>
                                            <option value="1">Si</option>
                                        </select>
                                    </td>
                                    <td><input name="return[]" type="checkbox" value=""></td>
                                    <td><a class="btn btn-primary btn-sm bgm-gray delete">Eliminar</a></td>
                                    </tr> -->
                     	</tbody>
                    </table>
                </div>
          	</div>

	<!-- 				<input name="product_id[]" type="hidden">
                    <input name="product_name[]" type="hidden">
                    <input name="boxes[]" type="hidden">
                    <input name="units[]" type="hidden">
					<input name="lot[]" type="hidden">                             
                     
                    <select name="without-charge[]" style="visibility:hidden">
                    	<option value="0">No</option>
						<option value="1">Si</option>
                    </select>
					<input name="return[]" type="hidden"> -->
                                    

       		<div class="card">
       		    <div class="card-header">
       		        <h2>Envio de albarán <small>Al finalizar el pedido dar al botón de enviar.</small></h2>
       		    </div>
       		    <div class="card-body card-padding">
       		        <div class="row">
       		            <div class="col-sm-4">
       		                <button id="submit-delivery" type="submit" class="btn btn-primary btn-sm m-t-5 bgm-gray">Enviar</button>
       		            </div>
       		        </div>
       		    </div>
       		</div>

            </form>

        </div>
    </div>

</section>


<script type='text/javascript'>
<?php
$js_array = json_encode($products);
echo "var products_js = ". $js_array . ";\n";
?>
//console.log(products_js);

</script>


@include('dashboard.layouts.footer')
