@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')


        <section id="content" style="padding-top: 50px; z-index: 3">
                <div class="container">
                    <div class="block-header">
                        <h2>Distribuidores</h2>
                    </div>
                
            <div class="card" style=" z-index: 1">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-sm-6 col-md-6">
                                    <h2>Lista de los distribuidores <small>Lista completa de los distribuidores registrados actualmente.</small></h2>
                                    <a href="{{ URL::to('/') }}/dealers/create"><button class="btn bgm-gray1" style="margin-top:20px ">Crear Nuevo distribuidor</button></a>
                                </div>
                                <div class="col-sm-6 col-md-4 col-md-offset-2">
                                    <div class="mini-charts-item bgm-bluegray" style=" margin:0px;margin-top: 25px">
                                        <div class="clearfix">
                                            <div class="chart stats-line-2"></div>
                                            <div class="count">
                                                <small>Total distribuidores</small>
                                                <h2>{!! $total !!}</h2>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    @if($dealers->isEmpty())



                    <div class="container" style="text-align: center; padding-top: 60px; padding-bottom: 60px">

                        <h3>Actualmente no hay ningun distribuidor registrado.</h3>
                    </div>

                    @else
                            
                        <table id="data-table-other" class="table table-striped table-vmiddle">
                            <thead>
                                <tr>
                                    <th data-column-id="id" data-type="numeric">ID</th>
                                    <th data-column-id="sender">Nombre</th>
                                    <th data-column-id="name">Usuario</th>
                                    <th data-column-id="date" data-order="desc">fecha creación</th>
                                    <th data-column-id="time" >Fecha actualización</th>
                                    <th data-column-id="commands" data-formatter="commands" data-sortable="false">Opciones</th>

                                </tr>
                            </thead>
                            <tbody>

                            @foreach( $dealers as $dealer )
                                <tr>
                                    <td>{!! $dealer->id !!}</td>
                                    <td>{!! $dealer->name !!}</td>
                                    <td>{!! $dealer->nickname !!}</td>
                                    <td>{!! $dealer->created_at !!}</td>
                                    <td>{!! $dealer->updated_at !!}</td>
                                </tr>
                            @endforeach
                                                 
                            </tbody>
                        </table>
                    @endif
                    </div>

            </div>
    </section>


@include('dashboard.layouts.footer')
