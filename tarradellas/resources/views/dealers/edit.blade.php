@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')


        <section id="content" style="padding-top: 50px; z-index: 3">
                <div class="container">
                    <div class="block-header">
                        <h2>Distribuidores</h2>
                    </div>

            <div class="card">
                        <div class="card-header">
                            <h2>Editar distribuidor<small> Rellene los campos para editar el distribuidor.</small></h2>
                        </div>
                        
                        <div class="card-body card-padding">
                            <form method="POST" action="{{URL::to('/')}}/dealers/{{$dealer->id}}">
                            <input name="_method" type="hidden" value="PUT">

                                {!! csrf_field() !!}

                                <div class="form-group fg-line">
                                    <label>Nombre / Referencia</label>
                                    <input type="text" class="form-control input-sm" id="name" name="name" placeholder="Nombre / Referencia" value="{{$dealer->name}}">
                                </div>
                                <div class="form-group fg-line">
                                    <label>Nombre de usuario ( cuenta de usuario, utilizado para el login del usuario )</label>
                                    <input type="text" class="form-control input-sm" id="nickname" name="nickname" placeholder="Nombre de Usuario" value="{{$dealer->nickname}}">
                                </div>
                                <div class="form-group fg-line">
                                    <label>Cambiar Contraseña</label>
                                    <input type="password" class="form-control input-sm" id="password" name="password" placeholder="Contraseña">
                                </div>

                                <input type="hidden" name='type' value='2'></input>

                                <button type="submit" class="btn btn-primary btn-sm m-t-10 bgm-gray1">Editar distribuidor</button>
                            </form>
                        </div>
                    </div>
            </div>
    </section>


@include('dashboard.layouts.footer')
