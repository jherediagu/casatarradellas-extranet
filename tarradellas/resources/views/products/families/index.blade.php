@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')


        <section id="content" style="padding-top: 50px; z-index: 3">
                <div class="container">
                    <div class="block-header">
                        <h2>Familias</h2>
                        
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h2>Añadir familias<small>Añadir familias a partir de un archivo .txt</small></h2>
                        </div>
                        
                        <div class="card-body card-padding">
                            <form method="POST" action="{{URL::to('/')}}/families" enctype="multipart/form-data">
                                {!! csrf_field() !!}
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <span class="btn btn-primary btn-file bgm-gray1">
                                            <span class="fileinput-new">Seleccionar archivo para enviar</span>
                                            <span class="fileinput-exists">Canviar el archivo cargado</span>
                                            <input type="file" name="families">
                                        </span>
                                        <span class="fileinput-filename"></span>
                                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                    </div>
                                </div>
                                
                                <div class="col-sm-4">
                                        <button type="submit" class="btn btn-primary btn-md bgm-gray1">Enviar</button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>

            <div class="card" style=" z-index: 1">

                        <div class="card-header">
                            <div class="row">
                                <div class="col-sm-6 col-md-6">
                                    <h2>Lista de Familias <small>Lista completa de las familias de productos registradas.</small></h2>
                                </div>
                                <div class="col-sm-6 col-md-4 col-md-offset-2">
                                    <div class="mini-charts-item bgm-bluegray" style=" margin:0px;margin-top: 25px">
                                        <div class="clearfix">
                                            <div class="chart stats-line-2"></div>
                                            <div class="count">
                                                <small>Total Familias</small>
                                                <h2>{!! $total !!}</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    

                    @if($families->isEmpty())



                    <div class="container" style="text-align: center; padding-top: 60px; padding-bottom: 60px">

                        <h3>Actualmente no hay ninguna familia registrada.</h3>
                    </div>

                    @else
                            
                        <table id="data-table-other" class="table table-striped table-vmiddle">
                            <thead>
                                <tr>
                                    <th data-column-id="id" data-type="numeric"  data-width="7%">ID</th>
                                    <th data-column-id="sender"  data-width="11%">id / familia</th>
                                    <th data-column-id="name"  data-width="40%">Nombre</th>
                                    <th data-column-id="date" data-order="desc">fecha creación</th>
                                    <th data-column-id="time" >Fecha actualización</th>
                                    <th data-column-id="commands" data-formatter="commands" data-sortable="false">Opciones</th>

                                </tr>
                            </thead>
                            <tbody>

                            @foreach( $families as $family )
                                <tr>
                                    <td>{!! $family->id !!}</td>
                                    <td>{!! $family->family_id !!}</td>
                                    <td>{!! $family->name !!}</td>
                                    <td>{!! $family->created_at !!}</td>
                                    <td>{!! $family->updated_at !!}</td>
                                </tr>
                            @endforeach
                                                 
                            </tbody>
                        </table>
                    @endif
                    </div>

            </div>
    </section>


@include('dashboard.layouts.footer')
