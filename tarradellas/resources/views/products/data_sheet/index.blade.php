@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')


        <section id="content" style="padding-top: 50px; z-index: 3">
                <div class="container">
                    <div class="block-header">
                        <h2>Fichas Técnica</h2>
                        
                    </div>
                    


                    <div class="card">
                        <div class="card-header">
                            <h2>Añadir fichas técnicas<small>Añadir fichas técnicas a partir de un archivo .txt</small></h2>
                        </div>
                        
                        <div class="card-body card-padding">
                            <form method="POST" action="{{URL::to('/')}}/products" enctype="multipart/form-data">
                                {!! csrf_field() !!}
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <span class="btn btn-primary btn-file bgm-gray1">
                                            <span class="fileinput-new">Seleccionar archivo para enviar</span>
                                            <span class="fileinput-exists">Canviar el archivo cargado</span>
                                            <input type="file" name="products">
                                        </span>
                                        <span class="fileinput-filename"></span>
                                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                    </div>
                                </div>
                                
                                <div class="col-sm-4">
                                        <button type="submit" class="btn btn-primary btn-md bgm-gray1">Enviar</button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>

            <div class="card" style=" z-index: 1">

                        <div class="card-header">
                            <div class="row">
                                <div class="col-sm-6 col-md-6">
                                    <h2>Lista de fichas técnicas <small>Lista completa de todas las fichas técnicas disponibles.</small></h2>
                                </div>
                                <div class="col-sm-6 col-md-4 col-md-offset-2">
                                    <div class="mini-charts-item bgm-bluegray" style=" margin:0px;margin-top: 25px">
                                        <div class="clearfix">
                                            <div class="chart stats-line-2"></div>
                                            <div class="count">
                                                <small>Total fichas técnicas</small>
                                                <h2>{!! $total !!}</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    

                    @if($products->isEmpty())



                    <div class="container" style="text-align: center; padding-top: 60px; padding-bottom: 60px">

                        <h3>Actualmente no hay ninguna ficha registrada.</h3>
                    </div>

                    @else
                            
                        <table id="data-table-other" class="table table-striped table-vmiddle">
                            <thead>
                                <tr>
                                    <th data-column-id="id" data-type="numeric" data-width="7%">ID</th>
                                    <th data-column-id="name" data-width="9%">id producto</th>
                                    <th data-column-id="sender" data-width="40%">Nombre</th>
                                    <th data-column-id="name_family" data-width="15%">Nombre comercial</th>
                                    <th data-column-id="date" data-order="desc">Fecha creación</th>
                                    <th data-column-id="commands" data-formatter="commands" data-sortable="false">Opciones</th>

                                </tr>
                            </thead>
                            <tbody>

                            @foreach( $products as $product )
                                <tr>
                                    <td>{!! $product->id !!}</td>
                                    <td>{!! $product->product_id !!}</td>
                                    <td>{!! $product->name !!}</td>
                                    <td>{!! $product->comercial_name !!}</td>
                                    <td>{!! $product->created_at !!}</td>
                                </tr>
                            @endforeach
                                                 
                            </tbody>
                        </table>
                    @endif
                    </div>

            </div>
    </section>


@include('dashboard.layouts.footer')
