@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')


<style type="text/css">
    
.dd > tbody > tr > td {

    padding:2px;
}

</style>
<section id="content" style="padding-top: 50px; z-index: 3">
    <div class="container">
        <div class="block-header">
            <h2>Ficha técnica</h2>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card" style=" z-index: 1">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <h2 style="padding-left: 20px">{!! $product->name!!}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     
        <div class="row">
            <div class="col-md-5">
                <div class="card" style=" z-index: 1">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <img style="width: 100%" src="http://extranet.casatarradellas.es/productos/{!!$product->product_id !!}.jpg">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" style=" z-index: 1">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <h2 style="font-size: 25px;text-align: center">FICHA LOGÍSTICA</h2>
                                <hr>
                                <div class="table-responsive">
                                    <table class="table table-striped dd">
                                        <tbody>
                                            <tr>
                                                <td>CÓDIGO</td>
                                                <td>{!! $product->logistics->product_id !!}</td>
                                            </tr>
                                            <tr>
                                                <td>EAN 13</td>
                                                <td>{!! $product->logistics->ean_code !!}</td>
                                            </tr>
                                            <tr>
                                                <td>DUN 14</td>
                                                <td>{!! $product->logistics->dun_code !!}</td>
                                            </tr>
                                            <tr>
                                                <th colspan="2"><br>PALET</th>
                                            </tr>
                                            <tr>
                                                <td>CAJAS / PALET</td>
                                                <td>{!! $product->logistics->pallet_box !!}</td>
                                            </tr>
                                            <tr>
                                                <td>CAJAS / CAPA</td>
                                                <td>{!! $product->logistics->pallet_box_layer !!}</td>
                                            </tr>
                                            <tr>
                                                <td>CAPAS / PALET</td>
                                                <td>{!! $product->logistics->pallet_layer !!}</td>
                                            </tr> 
                                            <tr>
                                                <td>ALTURA PALET</td>
                                                <td>{!! $product->logistics->pallet_height !!}</td>
                                            </tr><tr></tr> 
                                            <tr>
                                                <th colspan="2"><br>CAJA</th>
                                            </tr>
                                            <tr>
                                                <td>TARA CAJA</td>
                                                <td>{!! $product->logistics->box_tara !!}</td>
                                            </tr> 
                                            <tr>
                                                <td>UNIDADES / CAJA</td>
                                                <td>{!! $product->logistics->box_units !!}</td>
                                            </tr> 
                                            <tr>
                                                <td>UNIDADES / CAJA</td>
                                                <td>{!! $product->logistics->box_units_pack !!}</td>
                                            </tr>
                                            <tr>
                                                <th rowspan="3"><br>MEDIDAS</th>
                                                <td style="padding-left: 30px">LARGO {!! $product->logistics->box_long !!}</td>
                                            </tr> 
                                            <tr><td>ANCHO {!! $product->logistics->box_width !!}</td></tr>   
                                            <tr><td>ALTURA {!! $product->logistics->box_height !!}</td></tr>
                                            <tr>
                                                <td>PESO BRUTO</td>
                                                <td>{!! $product->logistics->box_gross_weight !!}</td>
                                            </tr>
                                            <tr>
                                                <td>PESO NETO</td>
                                                <td>{!! $product->logistics->box_net_weight !!}</td>
                                            </tr><tr></tr>
                                            <tr>
                                                <th colspan="2"><br>UNIDAD</th>
                                            </tr>
                                            <tr>
                                                <th rowspan="4"><br>MEDIDAS</th>
                                                <td style="padding-left: 30px">LARGO {!! $product->logistics->unit_long !!}</td>
                                            </tr> 
                                            <tr><td>ANCHO {!! $product->logistics->unit_width !!}</td></tr>   
                                            <tr><td>ALTURA {!! $product->logistics->unit_height !!}</td></tr>
                                            <tr><td>DIAMETRO {!! $product->logistics->unit_diameter !!}</td></tr>
                                            <tr>
                                                <td>PESO BRUTO</td>
                                                <td>{!! $product->logistics->unit_gross_weight !!}</td>
                                            </tr>
                                            <tr>
                                                <td>PESO NETO</td>
                                                <td>{!! $product->logistics->unit_net_weight !!}</td>
                                            </tr>
                                            <tr>
                                                <td>CADUCIDAD</td>
                                                <td>{!! $product->logistics->expiration !!}</td>
                                            </tr><tr></tr>                                                                   
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-7">
                <div class="card" style=" z-index: 1">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <h2 style="font-size: 25px;text-align: center">FICHA TÉCNICA</h2>
                                <hr><br>
                                <h2>DENOMINACIÓN DE VENTA</h2>
                                <p>{!! $product->comercial_name !!}</p>
                                <h2>CARATERÍSTICAS DEL ENVASE</h2>
                                <p>{!! $product->package_type !!}</p>
                                <h2>CONSERVACIÓN</h2>
                                <p>{!! $product->conservation !!}</p>
                                <h2>CONSUMO PREFERENTE / LOTE</h2>
                                <p>{!! $product->lot_expiration !!}</p>
                                <h2>CARATERÍSTICAS DEL PRODUCTO</h2>
                                <p>{!! $product->caract_prod !!}</p>
                                <h2>INGREDIENTES</h2>
                                <p>{!! $product->ingredients !!}</p>
                                <h2>CARATERÍSTICAS MICROBIOLÓGICAS</h2>
                                <p>{!! $product->caract_biolog !!}</p>
                                <h2>USO ESPERADO / MODO DE EMPLEO</h2>
                                <p>{!! $product->us_esperat !!}</p>
                                <h2>GRUPO DE CONSUMIDORES AL QUE SE DESTINA</h2>
                                <p>{!! $product->grup_consum !!}</p>
                                <h2>INFORMACIÓ NUTRICIONAL</h2>
                                <p>Valor medio en 100 g. de prod.</p>

                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <tbody>
                                            <tr>
                                                <td>Valor energético</td>
                                                <td>{!! $product->valor_energetic !!}</td>
                                            </tr>
                                            <tr>
                                                <td>Grasas</td>
                                                <td>{!! $product->grasas !!}</td>
                                            </tr>
                                            <tr>
                                                <td> (de las cuales saturadas)</td>
                                                <td>{!! $product->grasas_saturadas !!}</td>
                                            </tr>
                                            <tr>
                                                <td>Hidratos de carbono</td>
                                                <td>{!! $product->hidrats_carboni !!}</td>
                                            </tr>
                                            <tr>
                                                <td> (de los cuales azúcares)</td>
                                                <td>{!! $product->hidrats_carb_azucar !!}</td>
                                            </tr>
                                            <tr>
                                                <td>Proteínas</td>
                                                <td>{!! $product->proteines !!}</td>
                                            </tr>
                                            <tr>
                                                <td>Sal</td>
                                                <td>{!! $product->sal !!}</td>
                                            </tr><tr></tr>                                                                      
                                        </tbody>
                                    </table>
                                </div><br><br>

                                <div class="row">
                                    <div class="col-sm-5 col-md-5">
                                        <h2>REVISADO</h2>
                                        <p>{!! $product->review_date !!}</p>
                                    </div>
                                    <div class="col-sm-7 col-md-7">
                                        <h2>APROBADO</h2>
                                        <p>Dtor. General de Calidad y Medio Ambiente <br> Dtor. General de Operaciones Industriales</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

</section>


@include('dashboard.layouts.footer')
