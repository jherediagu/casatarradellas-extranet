@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')


        <section id="content" style="padding-top: 50px; z-index: 3">
                <div class="container">
                    <div class="block-header">
                        <h2>Listado de productos</h2>
                        
                    </div>
                    

            <div class="card" style=" z-index: 1">

                        <div class="card-header">
                            <div class="row">
                                <div class="col-sm-6 col-md-6">
                                    <h2>Lista de productos <small>Lista completa de productos distribuidos en familias.</small></h2>
                                </div>
                                <div class="col-sm-6 col-md-4 col-md-offset-2">
                                    <div class="mini-charts-item bgm-bluegray">
                                        <div class="clearfix">
                                            <div class="count">
                                                <small>Total productos</small>
                                                <h2>{!! $total !!}</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    @if($products->isEmpty() || $families->isEmpty())

                    <div class="container" style="text-align: center; padding-top: 60px; padding-bottom: 60px">

                        <h3>Actualmente no hay ningun producto o familia registrada</h3>
                    </div>

                    @else

                    <div class="card-body card-padding">

                         <div class="panel-group" role="tablist" aria-multiselectable="true">

                            @foreach ($families as $family)
                                <div class="panel panel-collapse">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#{!! $family->id !!}" aria-expanded="true" aria-controls="collapseOne">
                                                {!! $family->name !!}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="{!! $family->id !!}" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>id</th>
                                                            <th>id del producto</th>
                                                            <th>Nombre</th>
                                                            <th>Nombre comercial</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($products as $product)
                                                        @if($product->logistics['family_id'] == $family->family_id)
                                                            <tr onclick="window.document.location='{{URL::to('/')}}/products/{!! $product->id !!}'">
                                                                <td>{!! $product->id !!}</td>
                                                                <td>{!! $product->product_id !!}</td>
                                                                <td>{!! $product->name !!}</td>
                                                                <td>{!! $product->comercial_name !!}</td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    @endif
                    </div>

            </div>
    </section>


@include('dashboard.layouts.footer')
