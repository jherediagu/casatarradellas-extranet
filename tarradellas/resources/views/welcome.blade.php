<!DOCTYPE html>
    <!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Extranet Casa tarradellas</title>
        
        <!-- Vendor CSS -->
        <link href="{!! asset('assets/vendors/bower_components/animate.css/animate.min.css') !!}" rel="stylesheet">
        <link href="{!! asset('assets/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css') !!}" rel="stylesheet">
            
        <!-- CSS -->
        <link href="{!! asset('assets/css/app.min.1.css') !!}" rel="stylesheet">
        <link href="{!! asset('assets/css/app.css') !!}" rel="stylesheet">
    </head>
    
    <body class="login-content">

        <!-- Login -->
        <div class="lc-block toggled" id="l-login">
            <div class="row">
                <div class="col-md-12">
                    <img src="{!! asset('assets/img/logo.png') !!}" style="z-index: 4;"> 
                </div>
            </div>

<!-- <form method="POST" action="{{URL::to('/')}}/auth/register">
    {!! csrf_field() !!}

    <div>
        Name
        <input type="text" name="name" value="{{ old('name') }}">
    </div>

    <div>
        Nickname
        <input type="text" name="nickname" value="{{ old('nickname') }}">
    </div>


        <div>
        Email
        <input type="email" name="email" value="{{ old('email') }}">
    </div>

    <div>
        Password
        <input type="password" name="password">
    </div>

    <div>
        Confirm Password
        <input type="password" name="password_confirmation">
    </div>

    <div>
        <button type="submit">Register</button>
    </div>
</form> -->

            <form method="POST" action="{{URL::to('/')}}/auth/login" id="loginform">
                {!! csrf_field() !!}

                                    @if (count($errors) > 0)

                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>

                                @endif

                <h2>Casa tarradellas</h2>
                <small>Sistema de gestión de distribuidores</small>

                <div class="input-group m-b-20" style="padding-top: 20px">
                    <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                    <div class="fg-line">
                        <input type="text" name="nickname" class="form-control" placeholder="Nombre de usuario" value="{{ old('email') }}">
                    </div>
                </div>
                
                <div class="input-group m-b-20">
                    <span class="input-group-addon"><i class="zmdi zmdi-male"></i></span>
                    <div class="fg-line">
                        <input type="password" name="password" class="form-control" placeholder="Contraseña" value="{{ old('password') }}">
                    </div>
                </div>
                
                <div class="clearfix"></div>
                
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember" value="">
                        <i class="input-helper"></i>
                        Mantener la sessión
                    </label>
                </div>
                
                <button type="submit" class="btn btn-login btn-danger btn-float"><i class="zmdi zmdi-arrow-forward"></i></button>
                
                <ul class="login-navigation">
                    <li data-block="#l-forget-password" class="bgm-orange">Has perdido la contrasenya?</li>
                </ul>

            </form>
        </div>
        

        <!-- Forgot Password -->
        <div class="lc-block" id="l-forget-password">
            <p class="text-left"> Escribe tu email para recuperar la contraseña
            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
                <div class="fg-line">
                    <input type="text" class="form-control" placeholder="Email">
                </div>
            </div>
            
            <a href="" class="btn btn-login btn-danger btn-float"><i class="zmdi zmdi-arrow-forward"></i></a>
            
            <ul class="login-navigation">
                <li data-block="#l-login" class="bgm-green">Login</li>
            </ul>
        </div>
        
        <!-- Older IE warning message -->
        <!--[if lt IE 9]>
            <div class="ie-warning">
                <h1 class="c-white">Warning!!</h1>
                <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
                <div class="iew-container">
                    <ul class="iew-download">
                        <li>
                            <a href="http://www.google.com/chrome/">
                                <img src="img/browsers/chrome.png" alt="">
                                <div>Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.mozilla.org/en-US/firefox/new/">
                                <img src="img/browsers/firefox.png" alt="">
                                <div>Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com">
                                <img src="img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.apple.com/safari/">
                                <img src="img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                                <img src="img/browsers/ie.png" alt="">
                                <div>IE (New)</div>
                            </a>
                        </li>
                    </ul>
                </div>
                <p>Sorry for the inconvenience!</p>
            </div>   
        <![endif]-->
        
        <!-- Javascript Libraries -->
        <script src="{!! asset('assets/vendors/bower_components/jquery/dist/jquery.min.js') !!}"></script>
        <script src="{!! asset('assets/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js') !!}"></script>
        
        <script src="{!! asset('assets/vendors/bower_components/Waves/dist/waves.min.js') !!}"></script>
        
        <!-- Placeholder for IE9 -->
        <!--[if IE 9 ]>
            <script src="vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
        <![endif]-->
        
        <script src="{!! asset('assets/js/functions.js') !!}"></script>
        
    </body>
</html>