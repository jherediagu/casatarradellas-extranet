@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')


        <section id="content" style="padding-top: 50px; z-index: 3">
                <div class="container">
                    <div class="block-header">
                        <h2>Avisos</h2>
                    </div>
                
            <div class="card" style=" z-index: 1">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-sm-6 col-md-6">
                                    <h2>Avisos <small>Listado de noticias.</small></h2>
                                </div>
                                <div class="col-sm-6 col-md-4 col-md-offset-2">
                                    <div class="mini-charts-item bgm-bluegray" style=" margin:0px;margin-top: 25px">
                                        <div class="clearfix">
                                            <div class="chart stats-line-2"></div>
                                            <div class="count">
                                                <small>Total Avisos</small>
                                                <h2>{!! $total !!}</h2>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    @if($news->isEmpty())



                    <div class="container" style="text-align: center; padding-top: 60px; padding-bottom: 60px">

                        <h3>Actualmente no hay ningun aviso registrado.</h3>
                    </div>

                    @else
                            
                        <div class="card-body table-responsive">
                            <table class="table">
                            <thead>
                                <tr>
                                    <th>Título</th>
                                    <th>Fecha creación</th>
                                    <th>Fecha actualización</th>
                                    <th>Aviso visto</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody>

                            @foreach( $news as $new )
                                <tr>
                                    <td>{!! $new->relationnew->title !!}</td>
                                    <td>{!! $new->relationnew->created_at !!}</td>
                                    <td>{!! $new->relationnew->updated_at !!}</td>
                                    
                                    @if( $new->viewed == 0)
                                        <td><button style="padding: 1px 30px 1px 30px;margin-top: 4px" class="btn btn-danger"> No </button></td>
                                    @else
                                        <td><button style="padding: 1px 35px 1px 35px;margin-top: 4px" class="btn btn-success"> Si </button></td>
                                    @endif
                                    <td>
                                        <a href="{{ URL::to('/') }}/notice-dealer/{!!$new->relationnew->id!!}"><button class="btn bgm-gray1">Ver noticia</button></a>
                                    </td>
                                </tr>
                            @endforeach
                                                 
                            </tbody>
                        </table>
                    </div>
                    @endif
                    </div>

            </div>
    </section>


@include('dashboard.layouts.footer')
