@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')


<section id="content" style="padding-top: 50px; z-index: 3">
    <div class="container">
        <div class="block-header">
            <h2>Distribuidores</h2>
        </div>

        <div class="card">
            <div class="card-header">
                <h2>Editar Noticia<small> Rellene los campos para editar la noticia.</small></h2>
            </div>
             
            <form method="POST" action="{{URL::to('/')}}/news/{{$news->id}}">
            <input name="_method" type="hidden" value="PUT">
            {!! csrf_field() !!}

            <!-- Tabs -->
            <div class="card-body">
                <ul class="tab-nav tn-justified tn-icon" role="tablist">
                    <li role="presentation" class="active">
                        <a class="col-sx-4" href="#tab-1" aria-controls="tab-1" role="tab" data-toggle="tab">
                            <i class="zmdi zmdi-home icon-tab"></i> Noticia
                        </a>
                    </li>
                    <li role="presentation">
                        <a class="col-xs-4" href="#tab-2" aria-controls="tab-2" role="tab" data-toggle="tab">
                            <i class="zmdi zmdi-pin icon-tab"></i> Añadir Distribuidores
                        </a>
                    </li>   
                </ul>            
                <div class="tab-content p-20">
                    <div role="tabpanel" class="tab-pane animated fadeIn in active" id="tab-1">
                        <h2>Editar Noticia<br><small> Rellene los campos para editar la noticia.</small></h2><br>

                        <div class="form-group fg-line">
                            <label>Título de la noticia</label>
                            <input type="text" class="form-control input-sm" id="title" name="title" placeholder="Título" value="{!! $news->title !!}">
                        </div>

                        <p class="f-500 c-black m-b-20">Descripción de la noticia</p>
                            
                        <textarea class="html-editor" name="content">{!! $news->content !!}</textarea> 
                            
                    </div>
                                    
                    <div role="tabpanel" class="tab-pane animated fadeIn" id="tab-2">
                        
                    
                        <h2>Distribuidores<br><small> Selecciona los distribuidores que pueden ver la noticia.</small></h2><br>

                        
                        @if($dealers->isEmpty())

                            <div class="container" style="text-align: center; padding-top: 60px; padding-bottom: 60px">
                                <h3>Actualmente no hay ningun distribuidor registrado.</h3>
                            </div>

                        @else

                        <div class="table-responsive">
                            <div class="p-20">
                                <button id="select-all" class="btn btn-primary btn-sm m-t-10 bgm-gray1 ">Seleccionar todos</button>
                            </div>
                            <table id="data-table-selection" class="table table-striped">
                                <thead>
                                    <tr>
                                            <th data-column-id="check" data-formatter="check">check</th>

                                        <th data-column-id="id" data-type="numeric" data-identifier="true">ID</th>
                                        <th data-column-id="sender">Nombre</th>
                                        <th data-column-id="name">Usuario</th>
                                        <th data-column-id="date" data-order="desc">fecha creación</th>
                                        <th data-column-id="time" >Fecha actualización</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                @foreach( $dealers as $dealer )
                                    <tr>
                                        <td>
                                          
                                        </td>
                                        <td>{!! $dealer->id !!}</td>
                                        <td>{!! $dealer->name !!}</td>
                                        <td>{!! $dealer->nickname !!}</td>
                                        <td>{!! $dealer->created_at !!}</td>
                                        <td>{!! $dealer->updated_at !!}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            @endif
                        </div>
                        
                    </div>

                </div>
            </div>
                        
            <div class="p-20">
                <button type="submit" class="btn btn-primary btn-sm m-t-10 bgm-gray1 ">Editar Noticia y asignar distribuidores</button>
            </div>

        </div>

        </form>
    </div>
</section>



@include('dashboard.layouts.footer')
