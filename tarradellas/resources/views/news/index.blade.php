@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')


        <section id="content" style="padding-top: 50px; z-index: 3">
                <div class="container">
                    <div class="block-header">
                        <h2>Noticias</h2>
                    </div>
                
            <div class="card" style=" z-index: 1">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-sm-6 col-md-6">
                                    <h2>Noticias <small>Listado de noticias.</small></h2>
                                    <a href="{{ URL::to('/') }}/news/create"><button class="btn bgm-gray1" style="margin-top:20px ">Crear Nueva Noticia</button></a>
                                </div>
                                <div class="col-sm-6 col-md-4 col-md-offset-2">
                                    <div class="mini-charts-item bgm-bluegray" style=" margin:0px;margin-top: 25px">
                                        <div class="clearfix">
                                            <div class="chart stats-line-2"></div>
                                            <div class="count">
                                                <small>Total Noticias</small>
                                                <h2>{!! $total !!}</h2>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    @if($news->isEmpty())



                    <div class="container" style="text-align: center; padding-top: 60px; padding-bottom: 60px">

                        <h3>Actualmente no hay ninguna noticia registrada.</h3>
                    </div>

                    @else
                            
                        <table id="data-table-other" class="table table-striped table-vmiddle">
                            <thead>
                                <tr>
                                    <th data-column-id="id" data-type="numeric" data-width="7%">ID</th>
                                    <th data-column-id="title" data-width="15%">Título</th>
                                    <th data-column-id="content" data-width="35%">Contenido</th>
                                    <th data-column-id="created_at" data-width="13%">Fecha creación</th>
                                    <th data-column-id="updated_at" data-width="13%">Fecha actualización</th>

                                    <th data-column-id="commands" data-formatter="commands" data-sortable="false">Opciones</th>
                                </tr>
                            </thead>
                            <tbody>

                            @foreach( $news as $new )
                                <tr>
                                    <td>{!! $new->id !!}</td>
                                    <td>{!! $new->title !!}</td>
                                    <td>{!! $new->content !!}</td>
                                    <td>{!! $new->created_at !!}</td>
                                    <td>{!! $new->updated_at !!}</td>
                                </tr>
                            @endforeach
                                                 
                            </tbody>
                        </table>
                    @endif
                    </div>

            </div>
    </section>


@include('dashboard.layouts.footer')
