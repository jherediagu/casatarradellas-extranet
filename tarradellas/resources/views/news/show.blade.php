@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')

<section id="content" style="padding-top: 50px; z-index: 3">
    <div class="container">
        <div class="block-header">
            <h2>Aviso</h2>
        </div>
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header ch-alt">
                    <h2>{!! $news->title !!}</h2>
                </div>
                <div class="card-body card-padding">
                    {!! $news->content !!}
                </div>
            </div>
            <a href="{!! URL::to('/') !!}/notice-dealer"><button class="btn bgm-gray1">Volver atrás</button></a>
        </div>
    </div>
</section>

@include('dashboard.layouts.footer')
