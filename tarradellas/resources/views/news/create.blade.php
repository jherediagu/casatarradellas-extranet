@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')


        <section id="content" style="padding-top: 50px; z-index: 3">
                <div class="container">
                    <div class="block-header">
                        <h2>Noticias</h2>
                    </div>

            <div class="card">
                        <div class="card-header">
                            <h2>Creación de una nueva noticia<small> Rellene todos los campos para crear una nueva noticia.</small></h2>
                        </div>
                        
                        <div class="card-body card-padding">
                            <form method="POST" action="{{URL::to('/')}}/news">
                                {!! csrf_field() !!}

                                <div class="form-group fg-line">
                                    <label>Título de la noticia</label>
                                    <input type="text" class="form-control input-sm" id="title" name="title" placeholder="Título">
                                </div>

                         <!--    <div class="row">
                                <div class="col-sm-3 m-b-20">
                                    <div class="form-group fg-line">
                                        <label>Fecha</label>
                                        <input name="date" type="text" class="form-control input-mask" data-mask="00/00/0000" placeholder="ejemplo: 23/05/2016">
                                    </div>
                                </div>

                            </div>
 -->
                            <p class="f-500 c-black m-b-20">Descripción de la noticia</p>
                            
                            <textarea class="html-editor" name="content"></textarea> 
                            
                           

                            <button type="submit" class="btn btn-primary btn-sm m-t-10 bgm-gray1">Crear Noticia</button>

                            </form>
                        </div>
                    </div>
            </div>
    </section>


@include('dashboard.layouts.footer')
