@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')



        <section id="content" style="padding-top: 50px; z-index: 3">
                <div class="container">
                    <div class="block-header">
                        <h2>Dashboard</h2>
                                                
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h2>Albaranes recientes <small>Albaranes recientes</small></h2>
                                </div>
                                <div class="card-body m-t-0">
                                    <table class="table table-inner table-vmiddle">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Nombre cliente</th>
                                                <th>Fecha</th>
                                                <th style="width: 130px">nº de lineas</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ( $delivery_notes as $note )
                                            <tr>
                                                <td class="f-500 c-cyan">{!! $note->delivery_note_id !!}</td>
                                                <td>{!! $note->customers['name'] !!}</td>
                                                <td>{!! $note->created_at !!}</td>

                                                <td class="f-500 c-cyan" style="text-align: center">{!! $note->total !!}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div id="recent-items-chart" class="flot-chart"></div>
                            </div>
                        </div>
                    </div>

                   <div class="row">
                        <div class="col-sm-4">
                            <div class="card">
                                <div class="card-header">
                                    <h2>Albaranes enviados</h2>
                                    <h2 style="font-weight: bold; font-size: 25px; padding-top: 10px">{!! $total_validated !!} Albaranes enviados</h2>
                                    <small>Albaranes recibidos y validados</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="card">
                                <div class="card-header">
                                    <h2>Albaranes no enviados</h2>
                                    <h2 style="font-weight: bold; font-size: 25px; padding-top: 10px">{!! $total_no_validated !!} Albaranes no validados</h2>
                                    <small>Albaranes no validados</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="card">
                                <div class="card-header">
                                    <h2>Productos</h2>
                                    <h2 style="font-weight: bold; font-size: 25px; padding-top: 10px">{!! $products_total !!} productos</h2>
                                    <small>Total de productos actuales</small>
                                </div>
                            </div>
                        </div>
                    </div>

                    
<!--                     <div class="mini-charts">
                        <div class="row">
                            <div class="col-sm-6 col-md-3">
                                <div class="mini-charts-item bgm-cyan">
                                    <div class="clearfix">
                                        <div class="chart stats-bar"></div>
                                        <div class="count">
                                            <small>Pedidos</small>
                                            <h2>1000</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-3">
                                <div class="mini-charts-item bgm-lightgreen">
                                    <div class="clearfix">
                                        <div class="chart stats-bar-2"></div>
                                        <div class="count">
                                            <small>Albaranes Procesados</small>
                                            <h2>100</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-3">
                                <div class="mini-charts-item bgm-orange">
                                    <div class="clearfix">
                                        <div class="chart stats-line"></div>
                                        <div class="count">
                                            <small>Albaranes Pendientes</small>
                                            <h2>23</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-3">
                                <div class="mini-charts-item bgm-bluegray">
                                    <div class="clearfix">
                                        <div class="chart stats-line-2"></div>
                                        <div class="count">
                                            <small>Albaranes Incorrectos</small>
                                            <h2>3</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                     -->
                    
<!--                     <div class="dash-widgets">
                        <div class="row">
                            <div class="col-md-3 col-sm-6">
                                <div id="site-visits" class="dash-widget-item bgm-teal">
                                    <div class="dash-widget-header">
                                        <div class="p-20">
                                            <div class="dash-widget-visits"></div>
                                        </div>
                                        
                                        <div class="dash-widget-title">For the past 30 days</div>
                                        
                                        <ul class="actions actions-alt">
                                            <li class="dropdown">
                                                <a href="" data-toggle="dropdown">
                                                    <i class="zmdi zmdi-more-vert"></i>
                                                </a>
                                                
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li>
                                                        <a href="">Refresh</a>
                                                    </li>
                                                    <li>
                                                        <a href="">Manage Widgets</a>
                                                    </li>
                                                    <li>
                                                        <a href="">Widgets Settings</a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                    
                                    <div class="p-20">
                                        
                                        <small>Page Views</small>
                                        <h3 class="m-0 f-400">47,896,536</h3>
                                        
                                        <br/>
                                        
                                        <small>Site Visitors</small>
                                        <h3 class="m-0 f-400">24,456,799</h3>
                                        
                                        <br/>
                                        
                                        <small>Total Clicks</small>
                                        <h3 class="m-0 f-400">13,965</h3>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-3 col-sm-6">
                                <div id="pie-charts" class="dash-widget-item">
                                    <div class="bgm-pink">
                                        <div class="dash-widget-header">
                                            <div class="dash-widget-title">Email Statistics</div>
                                        </div>
                                        
                                        <div class="clearfix"></div>
                                        
                                        <div class="text-center p-20 m-t-25">
                                            <div class="easy-pie main-pie" data-percent="75">
                                                <div class="percent">45</div>
                                                <div class="pie-title">Total Emails Sent</div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="p-t-20 p-b-20 text-center">
                                        <div class="easy-pie sub-pie-1" data-percent="56">
                                            <div class="percent">56</div>
                                            <div class="pie-title">Bounce Rate</div>
                                        </div>
                                        <div class="easy-pie sub-pie-2" data-percent="84">
                                            <div class="percent">84</div>
                                            <div class="pie-title">Total Opened</div>
                                        </div>
                                    </div>
    
                                </div>
                            </div>
                            
                        <div class="col-md-6 col-sm-6">
                            <div id="calendar-widget"></div>

                        </div>
    
                 
                        </div>
                    </div> -->
                
            </section>
        </section>




@include('dashboard.layouts.footer')
