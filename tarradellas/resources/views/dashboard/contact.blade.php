@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')


        <section id="content" style="padding-top: 50px; z-index: 3">
                <div class="container">
                    <div class="block-header">
                        <h2>Distribuidores</h2>
                    </div>
                
            <div class="card" style=" z-index: 1">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-sm-6 col-md-6">
                                    <h2>Formulario de contacto</h2>
                                </div>
                                <div class="col-sm-6 col-md-4 col-md-offset-2">
                                    <div class="mini-charts-item bgm-bluegray" style=" margin:0px;margin-top: 25px">
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body card-padding">
                        <div class="row">

                        <form method="POST" action="{{URL::to('/')}}/contact/send" enctype="multipart/form-data">
                            {!! csrf_field() !!}

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="fg-line">
                                    <label>Referencia:</label>
                                        <input name="reference" type="text" class="form-control" placeholder="Referencia" value="{!! Auth::user()->nickname !!}">
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="fg-line">
                                    <label>Nombre:</label>
                                        <input name="name" type="text" class="form-control" placeholder="Nombre" value="{!! Auth::user()->name !!}">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                    <label>Departamento:</label>
                                    
                                    <select name="email" class="select2" style="width: 100%">
                                        <option value="ddomenech@casatarradellas.es">Administración</option>
                                        <option value="xlozano@casatarradellas.es">Logística</option>
                                        <option value="etarradellas@casatarradellas.es">Marketing</option>
                                        <option value="anna.puig@casatarradellas.es">Informática</option>
                                        <option value="comercial@casatarradellas.es">Comercial</option>
                                        <option value="dfornols@casatarradellas.es">Finanzas</option>
                                        <option value="amolist@casatarradellas.es">Expediciones</option>
                                    </select>
                                </div>

                                <div class="col-md-5" style="padding-top: 22px">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <span class="btn btn-primary btn-file bgm-gray1">
                                            <span class="fileinput-new">Seleccionar archivo para enviar</span>
                                            <span class="fileinput-exists">Canviar el archivo cargado</span>
                                            <input type="file" name="customers_dealers">
                                        </span>
                                        <span class="fileinput-filename"></span>
                                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                    </div>
                                </div>
                                
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <label>Consulta:</label>
                                            <textarea name="content" class="form-control" rows="5" placeholder="Escribe aqui tu consulta"></textarea>
                                        </div>
                                    </div>
                                </div>
                                

                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-sm m-t-5 bgm-gray1">Enviar</button>
                            </div>
                        </div>

                        </form>

                    </div>
            </div>

            </div>
    </section>


@include('dashboard.layouts.footer')
