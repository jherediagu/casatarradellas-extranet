        <footer id="footer">
            Copyright &copy; {{ date('Y') }} - JAVAJAN - CASA TARRADELLAS
            
            <ul class="f-menu">
                <li><a href="{{URL::to('/')}}">Dashboard</a></li>
                <li><a href="mailto:joanh@javajan.com">Soporte</a></li>
                <li><a href="mailto:joanh@javajan.com">Contacto</a></li>
            </ul>
        </footer>

        <!-- Page Loader -->
<!--         <div class="page-loader">
            <div class="preloader pls-blue">
                <svg class="pl-circular" viewBox="25 25 50 50">
                    <circle class="plc-path" cx="50" cy="50" r="20" />
                </svg>

                <p>Please wait...</p>
            </div>
        </div>
 -->    
        <!-- Older IE warning message -->
        <!--[if lt IE 9]>
            <div class="ie-warning">
                <h1 class="c-white">Warning!!</h1>
                <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
                <div class="iew-container">
                    <ul class="iew-download">
                        <li>
                            <a href="http://www.google.com/chrome/">
                                <img src="img/browsers/chrome.png" alt="">
                                <div>Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.mozilla.org/en-US/firefox/new/">
                                <img src="img/browsers/firefox.png" alt="">
                                <div>Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com">
                                <img src="img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.apple.com/safari/">
                                <img src="img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                                <img src="img/browsers/ie.png" alt="">
                                <div>IE (New)</div>
                            </a>
                        </li>
                    </ul>
                </div>
                <p>Sorry for the inconvenience!</p>
            </div>   
        <![endif]-->
        
        <!-- Javascript Libraries -->
        <script src="{!! asset('assets/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js') !!}"></script>

        <script src="{!! asset('assets/vendors/bower_components/jquery/dist/jquery.min.js') !!}"></script>
        <script src="{!! asset('assets/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js') !!}"></script>
        
        <script src="{!! asset('assets/vendors/bower_components/flot/jquery.flot.js') !!}"></script>
        <script src="{!! asset('assets/vendors/bower_components/flot/jquery.flot.resize.js') !!}"></script>
        <script src="{!! asset('assets/vendors/bower_components/flot.curvedlines/curvedLines.js') !!}"></script>
        <script src="{!! asset('assets/vendors/sparklines/jquery.sparkline.min.js') !!}"></script>
        <script src="{!! asset('assets/vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js') !!}"></script>
        
        <script src="{!! asset('assets/vendors/bower_components/moment/min/moment.min.js') !!}"></script>
        <script src="{!! asset('assets/vendors/bower_components/fullcalendar/dist/fullcalendar.min.js') !!}"></script>
        <script src="{!! asset('assets/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js') !!}"></script>
        <script src="{!! asset('assets/vendors/bower_components/Waves/dist/waves.min.js') !!}"></script>
        <script src="{!! asset('assets/vendors/bootstrap-growl/bootstrap-growl.min.js') !!}"></script>
        <script src="{!! asset('assets/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') !!}"></script>
        
        <script src="{!! asset('assets/vendors/bootgrid/jquery.bootgrid.updated.min.js') !!}"></script>

        <!-- Placeholder for IE9 -->
        <!--[if IE 9 ]>
            <script src="vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
        <![endif]-->
        
        <script src="{!! asset('assets/js/flot-charts/curved-line-chart.js') !!}"></script>
        <script src="{!! asset('assets/js/flot-charts/line-chart.js') !!}"></script>
        <script src="{!! asset('assets/js/charts.js') !!}"></script>
        
        <script src="{!! asset('assets/js/charts.js') !!}"></script>
        <script src="{!! asset('assets/js/functions.js') !!}"></script>

        <script src="{!! asset('assets/vendors/input-mask/input-mask.min.js') !!}"></script>
        <script src="{!! asset('assets/vendors/summernote/dist/summernote-updated.min.js') !!}"></script>

        <script src="{!! asset('assets/vendors/bower_components/chosen/chosen.jquery.min.js') !!}"></script>
        <script src="{!! asset('assets/vendors/fileinput/fileinput.min.js') !!}"></script>


        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>        

    <script type="text/javascript">
    $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
    });
</script>
        <!-- Data Table -->
        <script type="text/javascript">
            $(document).ready(function(){

                var k = 1;
                // Listen for click on toggle checkbox
                $('#select-all').click(function(event) { 
                event.preventDefault();

                    if (k == 1) {
                        // Iterate each checkbox
                        $(':checkbox').each(function() {
                            this.checked = true; 
                            k = 2;                       
                        });

                    }else{
                        k =1 ;
                        $(':checkbox').each(function() {
                            this.checked = false;                        
                        });
                    }
                
                });

                //Command Buttons
                $("#data-table-command").bootgrid({
                    css: {
                        icon: 'zmdi icon',
                        iconColumns: 'zmdi-view-module',
                        iconDown: 'zmdi-expand-more',
                        iconRefresh: 'zmdi-refresh',
                        iconUp: 'zmdi-expand-less'
                    },
                    formatters: {
                        "commands": function(column, row) {
                            return "<a  style=\"color:#646464;text-decoration: none;\" class=\"btn btn-icon command-edit waves-effect waves-circle\" href=\"{!!Route::getCurrentRoute()->getPath()!!}/"+row.id+"/edit\"><span class=\"zmdi zmdi-edit\"></span></a> " + 
                                " <button type=\"button\" class=\"btn btn-icon command-delete waves-effect waves-circle\" data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-lock\"></span></button> "+
                                  " <a  style=\"color: #fff;text-decoration: none;\" class=\"btn btn-icon command-delete waves-effect waves-circle bgm-gray1\" href=\"{!!Route::getCurrentRoute()->getPath()!!}/delete/"+row.id+"\"><span class=\"zmdi zmdi-delete\"></span></a>";
                        }
                    }
                });



                //Command Buttons
                $("#data-table-other").bootgrid({
                    css: {
                        icon: 'zmdi icon',
                        iconColumns: 'zmdi-view-module',
                        iconDown: 'zmdi-expand-more',
                        iconRefresh: 'zmdi-refresh',
                        iconUp: 'zmdi-expand-less'
                    },
                    formatters: {
                        "commands": function(column, row) {
                            return "<a  style=\"color:#646464;text-decoration: none;\" class=\"btn btn-icon command-edit waves-effect waves-circle\" href=\"{!!Route::getCurrentRoute()->getPath()!!}/"+row.id+"/edit\"><span class=\"zmdi zmdi-edit\"></span></a><a  style=\"color: #fff;text-decoration: none;\" class=\"btn btn-icon command-delete waves-effect waves-circle bgm-gray1\" href=\"{!!Route::getCurrentRoute()->getPath()!!}/delete/"+row.id+"\"><span class=\"zmdi zmdi-delete\"></span></a>";
                        }
                    }
                });


                //Command Buttons
                $("#data-table-delivery").bootgrid({
                    css: {
                        icon: 'zmdi icon',
                        iconColumns: 'zmdi-view-module',
                        iconDown: 'zmdi-expand-more',
                        iconRefresh: 'zmdi-refresh',
                        iconUp: 'zmdi-expand-less'
                    },
                    formatters: {
                        "commands": function(column, row) {
                            return "<a  style=\"color:#646464;text-decoration: none;\" class=\"btn btn-icon command-edit waves-effect waves-circle\" href=\"{!!Route::getCurrentRoute()->getPath()!!}/"+row.id+"/edit\"><span class=\"zmdi zmdi-edit\"></span></a> <a  style=\"color: #fff;text-decoration: none;\" class=\"btn btn-icon command-delete waves-effect waves-circle bgm-gray1\" href=\"{!!Route::getCurrentRoute()->getPath()!!}/delete/"+row.id+"\"><span class=\"zmdi zmdi-delete\"></span></a>  <a  style=\"color: #fff;text-decoration: none;\" class=\"btn btn-icon command-delete waves-effect waves-circle bgm-gray1\" href=\"{!!Route::getCurrentRoute()->getPath()!!}/file/"+row.id+"\"><span class=\"zmdi zmdi-download\"></span></a>";
                        }
                    }
                });


                //Command Buttons
                $("#data-table-delivery-note").bootgrid({
                    css: {
                        icon: 'zmdi icon',
                        iconColumns: 'zmdi-view-module',
                        iconDown: 'zmdi-expand-more',
                        iconRefresh: 'zmdi-refresh',
                        iconUp: 'zmdi-expand-less'
                    },
                    formatters: {
                        "commands": function(column, row) {
                            //console.log("row: "+JSON.stringify(row.validated));
                            //console.log("row: "+JSON.stringify(row));

                            if ((row.validated == 'Si') && (row.send == 'Si')) {
                                return "<a  style=\"color: #fff;text-decoration: none;\" class=\"btn btn-icon command-delete waves-effect waves-circle bgm-gray1\" href=\"{!!Route::getCurrentRoute()->getPath()!!}/pdf/"+row.id+"\"><span class=\"zmdi zmdi-file-text\"></span></a>";

                            }else{
                                return "<a  style=\"color:#646464;text-decoration: none;\" class=\"btn btn-icon command-edit waves-effect waves-circle\" href=\"{!!Route::getCurrentRoute()->getPath()!!}/"+row.deliverynotesid+"/edit\"><span class=\"zmdi zmdi-edit\"></span></a> <a  style=\"color: #fff;text-decoration: none;\" class=\"btn btn-icon command-delete waves-effect waves-circle bgm-gray1\" href=\"{!!Route::getCurrentRoute()->getPath()!!}/check/"+row.deliverynotesid+"\"><span class=\"zmdi zmdi-check\"></span></a> <a  style=\"color: #fff;text-decoration: none;\" class=\"btn btn-icon command-delete waves-effect waves-circle bgm-gray1\" href=\"{!!Route::getCurrentRoute()->getPath()!!}/delete/"+row.deliverynotesid+"\"><span class=\"zmdi zmdi-delete\"></span></a> <a  style=\"color: #fff;text-decoration: none;\" class=\"btn btn-icon command-delete waves-effect waves-circle bgm-gray1\" href=\"{!!Route::getCurrentRoute()->getPath()!!}/pdf/"+row.deliverynotesid+"\"><span class=\"zmdi zmdi-file-text\"></span></a>";
                            }
                            
                        }
                    }
                });

                //Data tabe for dealers --> delivery notes
                $("#data-table-dealer-delivery").bootgrid({
                    css: {
                        icon: 'zmdi icon',
                        iconColumns: 'zmdi-view-module',
                        iconDown: 'zmdi-expand-more',
                        iconRefresh: 'zmdi-refresh',
                        iconUp: 'zmdi-expand-less'
                    },
                });

        
        <?php if (isset($relation)) { ?>
                <?php
                $js_array = json_encode($relation);
                echo "var relation_js = ". $js_array . ";\n";
                ?>

                //Selection
                $("#data-table-selection").bootgrid({
                    css: {
                        icon: 'zmdi icon',
                        iconColumns: 'zmdi-view-module',
                        iconDown: 'zmdi-expand-more',
                        iconRefresh: 'zmdi-refresh',
                        iconUp: 'zmdi-expand-less'
                    },
                        formatters: {

                       "check": function(column, row) {

                            if (row.id == relation_js[row.id]) {

                                 return "<div class='checkbox m-b-15'><label><input type='checkbox' class='selection_dealers' name='selection_dealers[]' value='" + row.id + "' checked><i class='input-helper'></i></label></div>";

                            }else{

                                return "<div class='checkbox m-b-15'><label><input type='checkbox' class='selection_dealers' name='selection_dealers[]' value='" + row.id + "'><i class='input-helper'></i></label></div>";
                            }
                            
                        }
                    }

                });

            <?php } ?>



            });
        </script>

        @if(Session::has('success'))
            <script type="text/javascript">

            $(document).ready(function(){
                swal("{{ Session::get('success') }}", "Acepta para continuar", "success");
            });

            </script>      
        @endif


        @if(Session::has('error'))
            <script type="text/javascript">

            $(document).ready(function(){
                swal("{{ Session::get('error') }}", "Acepta para continuar", "error");
            });

            </script>      
        @endif
        

<script type="text/javascript">

    $(document).ready(function() {
      $(".select2").select2();
      $(".select2products").select2({});


       $(document).on('keyup', ".boxes", function() {
            var box_units = parseInt($( this ).attr('boxesunit'));
            var boxes = parseInt($( this ).val());
            var product_id = $( this ).attr('product_id');
            var total = box_units * boxes;

        $("."+product_id).attr('value',total);

        //console.log("box-units --> "+box_units+" boxes -->"+boxes+" productid -->"+product_id+" total -->"+total)


        });
    });

    jQuery(function(){
        var counter = 1;
        jQuery('#add-product').click(function(event){

            //var product_name = $( "#select-product option:selected" ).text();
            var product_name    = $( "#select-product option:selected" ).attr('nameproduct');
            var product_id      = $( "#select-product option:selected" ).attr('idproduct');
            var box_units       = $( "#select-product option:selected" ).attr('boxunits');

            event.preventDefault();
            counter++;
            var newRow = jQuery('<tr>'+
                                    '<td><input name="product_id'+counter+'"   type="hidden" value="'+product_id+'">'+product_id+'</td>'+
                                    '<td><input name="product_name'+product_id+'" type="hidden" value="'+product_name+'">'+product_name+'</td>'+
                                    '<td><input product_id="'+product_id+'"    boxesunit="'+box_units+'" class="boxes" name="boxes'+product_id+'" type="number" placeholder="cajas"></td>'+
                                    '<td><input class="'+product_id+'" name="units'+product_id+'" placeholder="unidades" readonly></td>'+
                                    '<td><input name="lot'+product_id+'" type="number"  placeholder="lote" required></td>'+
                                    '<td>'+                             
                                        '<select name="company_id'+product_id+'" class="select2" style="width: 100%">'+
                                            '<option value="0">No</option>'+
                                            '<option value="1">Si</option>'+
                                        '</select>'+
                                    '</td>'+
                                    //'<td><input name="return'+product_id+'"type="checkbox"></td>'+
                                    '<td><a class="btn btn-primary btn-sm bgm-gray delete">Eliminar</a></td>'+
                                    '</tr>');

            jQuery('#table-products').append(newRow);
        });
    });

    $(document).on('click', "a.delete", function() {
          $(this).parent().parent().fadeTo(400, 0, function () { 
            $(this).remove();
        });
        return false;       
    });


    $(document).on('click', "#submit-delivery", function(event) {
     
        event.preventDefault();

        var products = [];
                        
        $("#table-products  tbody  tr").each(function (i, index) {

            if ( i> 0) {

                var delivery_date   = $("input[name=deliverynote_date]").val();
                var order_date      = $("input[name=order_date]").val();
                var select_customer = $("#select_customer").val();
                var order_number    = $("input[name=order_number]").val();
                var observations    = $("textarea[name=observations]").val();

                var product_id      = $(index).find("td:eq(0) input").val();
                var product_name    = $(index).find("td:eq(1) input").val();
                var boxes           = $(index).find("td:eq(2) input").val();
                var units           = $(index).find("td:eq(3) input").val();
                var lot             = $(index).find("td:eq(4) input").val();
                var charge          = $(index).find("td:eq(5) select").val();
                //var return_product  = $(index).find("td:eq(6) input[type='checkbox']:checked").val();

                products.push([delivery_date,order_date,select_customer,order_number,observations,
                                product_id, product_name, boxes, units, lot, charge]);
        
            }             
        
        });

        console.log(products);

        $.ajax({
            type: "POST",
              headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
              dataType: 'json',
            url: "{{URL::to('/')}}/delivery-notes/create-delivery-note",
            data:{ products: products }, 
             success: function success(data, status) {
                window.location.href = "{{URL::to('/')}}/delivery-notes-dealer-manual/correct";
              // console.log(data);
              // alert(data);
          },
          error: function error(xhr, textStatus, errorThrown) {
                swal("No se ha finalizado correctamente el pedido", "Acepta para continuar", "error");
          }

           
        })

       
    });

//     $(document).on('click', '#send', function(){
//     var dataArr = [];
//     $("table").each(function(){
//         dataArr.push($(this).html());
//     });
//     $('#send').click(function(){
//         $.ajax({
//           type : "POST",
//           url : '{{URL::to('/')}}/delivery-notes/create-delivery-note',
//           data : "content="+dataArr,
//           success: function(data) {
//               alert(data);// alert the data from the server
//           },
//           error : function() {
//           }
//          });
//     });
// });

    </script>


    </body>
  </html>