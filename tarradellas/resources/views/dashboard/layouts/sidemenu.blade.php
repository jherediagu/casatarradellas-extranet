          <img src="{!! asset('assets/img/logo.png') !!}" style="  margin-left: 80px; z-index: 4; position:fixed "> 

        <header id="header" class="clearfix" data-current-skin="bluegray"  style=" margin-top:30px; z-index: 3; ">
            <ul class="header-inner">
                <li id="menu-trigger" data-trigger="#sidebar">
                    <div class="line-wrap">
                        <div class="line top"></div>
                        <div class="line center"></div>
                        <div class="line bottom"></div>
                    </div>
                </li>

                <li class="logo hidden-xs" style="padding-left: 190px">
                    <a href="{{ URL::to('/')}}/dashboard">Casa tarradellas <small> Sistema de gestión</small></a>
                </li>

                <li class="pull-right">
                    <ul class="top-menu">

             @if (Auth::check())

                @if (Auth::user()->type == 2)

                   <li class="dropdown">
                            <a href="{{ URL::to('/')}}/contact">
                                <i class="tm-icon zmdi zmdi-email"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a data-toggle="dropdown" href="">
                                <i class="tm-icon zmdi zmdi-notifications"></i>

                                <?php $count = 0 ?>

                                @foreach ($relation as $notification)

                                    @if($notification->id_dealer === Auth::user()->id && $notification->viewed == 0)

                                    <?php $count++; ?>

                                    @endif

                                @endforeach

                                @if ($count > 0)
                                    <i class="tmn-counts">{{$count}}</i>
                                @endif
                            </a>
                            <div class="dropdown-menu dropdown-menu-lg pull-right">
                                <div class="listview" id="notifications">
                                    <div class="lv-header">
                                        Notificaciones
                                    </div>
                                    <div class="lv-body">

                                    @if ($count != 0)
                
                                        @foreach ($relation as $notification)

                                            @if($notification->id_dealer == Auth::user()->id && $notification->viewed == 0)

                                                <a class="lv-item" href="{{URL::to('/')}}/notice-dealer/{!!$notification->relationnew->id!!}">
                                                    <div class="media">
                                                        <div class="media-body">
                                                            <div class="lv-title">{{$notification->relationnew->title}}</div>
                                                            <small class="lv-small">fecha notificacion: {{$notification->created_at}}</small>
                                                        </div>
                                                    </div>
                                                </a>

                                            @endif

                                        @endforeach

                                    @else

                                        <a class="lv-item" href="{{URL::to('/')}}/news">
                                            <div class="media">
                                                <div class="media-body">
                                                    <h4 style="text-align: center">No hay notificaciones disponibles</h4>
                                                </div>
                                            </div>
                                        </a>

                                    @endif

                                    </div>

                                    <a class="lv-footer" href="{{URL::to('/')}}/notice-dealer">Ver todas</a>
                                </div>

                            </div>
                        </li>

                @endif

            @endif    

                        <li class="dropdown">
                            <a data-toggle="dropdown" href=""><i class="tm-icon zmdi zmdi-more-vert"></i></a>
                            <ul class="dropdown-menu dm-icon pull-right">
                                <li class="hidden-xs">
                                    <a data-action="fullscreen" href=""><i class="zmdi zmdi-fullscreen"></i> Pantalla completa</a>
                                </li>
                                <li>
                                    <a href="{{URL::to('/')}}/auth/logout"><i class="zmdi zmdi-settings"></i> Salir</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>

            </ul>

        </header>

        <section id="main" data-layout="layout-1">
            <aside id="sidebar" class="sidebar c-overflow" style="margin-top: 36px; z-index: 2">

                <div style="text-align: center; padding-top: 20px"></div>

        @if (Auth::check())

            @if (Auth::user()->type == 0)
                
                <ul class="main-menu">
                    <li class="active">
                        <a href="{{ URL::to('/') }}/dashboard"><i class="zmdi zmdi-home"></i> Dashboard</a>
                    </li>

                    <li><a href="{{ URL::to('/') }}/dealers"><i class="zmdi zmdi-collection-text"></i> Distribuidores</a></li>
                    <li><a href="{{ URL::to('/') }}/customers"><i class="zmdi zmdi-collection-text"></i> Clientes</a></li>
                    <li><a href="{{ URL::to('/') }}/news"><i class="zmdi zmdi-collection-text"></i> Noticias</a></li>
                    <li><a href="{{ URL::to('/') }}/delivery-notes"><i class="zmdi zmdi-collection-text"></i> Albaranes</a></li>
                    <!-- <li><a href="typography.html"><i class="zmdi zmdi-collection-text"></i> Facturación</a></li> -->
                    <li><a href="{{ URL::to('/') }}/customers-dealers"><i class="zmdi zmdi-collection-text"></i> Clientes distribuidores</a></li>
                    
                    <li class="sub-menu">
                        <a href=""><i class="zmdi zmdi-collection-text"></i> Productos</a>

                        <ul>
                            <li><a href="{{ URL::to('/') }}/products">Listado</a></li>
                            <li><a href="{{ URL::to('/') }}/families">Famílias</a></li>
                            <li><a href="{{ URL::to('/') }}/logistics">Ficha logística</a></li>
                            <li><a href="{{ URL::to('/') }}/data-sheet">Ficha técnica</a></li>
                        </ul>
                    </li>
                </ul>

            @elseif(Auth::user()->type == 2)

                            <ul class="main-menu">
                    <li class="active">
                        <a href="{{ URL::to('/') }}/dashboard"><i class="zmdi zmdi-home"></i> Dashboard</a>
                    </li>
                    <li><a href="{{ URL::to('/') }}/delivery-notes-dealer"><i class="zmdi zmdi-collection-text"></i> Albaranes</a></li>
              <!--  <li><a href="{{ URL::to('/') }}/orders-dealer"><i class="zmdi zmdi-collection-text"></i> Pedidos</a></li>-->
<!--                     <li><a href="{{ URL::to('/') }}/query-dealer"><i class="zmdi zmdi-collection-text"></i> Consultas</a></li>
 -->                    <li><a href="{{ URL::to('/') }}/notice-dealer"><i class="zmdi zmdi-collection-text"></i> Avisos</a></li>
                    <li><a href="{{ URL::to('/') }}/products"><i class="zmdi zmdi-collection-text"></i> Productos</a></li>
                    <li><a href="{{ URL::to('/') }}/contact"><i class="zmdi zmdi-collection-text"></i> Contacto</a></li>
                </ul>


             @elseif(Auth::user()->type == 3)

                <ul class="main-menu">
                    <li class="active">
                        <a href="{{ URL::to('/') }}/dashboard"><i class="zmdi zmdi-home"></i> Dashboard</a>
                    </li>

                    <li><a href="{{ URL::to('/') }}/products-customer"><i class="zmdi zmdi-collection-text"></i> Productos</a></li>
                    <li><a href="{{ URL::to('/') }}/contact"><i class="zmdi zmdi-collection-text"></i> Contacto</a></li>
                </ul>
            
            @endif


        @endif
            </aside>
            