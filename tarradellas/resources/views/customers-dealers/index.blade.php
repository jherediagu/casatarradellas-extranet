@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')


        <section id="content" style="padding-top: 50px; z-index: 3">
                <div class="container">
                    <div class="block-header">
                        <h2>Clientes distribuidores</h2>
                        
                    </div>
                    


                    <div class="card">
                        <div class="card-header">
                            <h2>Añadir clientes distribuidores<small>Añadir clientes distribuidores a partir de un archivo .txt</small></h2>
                        </div>
                        
                        <div class="card-body card-padding">
                            <form method="POST" action="{{URL::to('/')}}/customers-dealers" enctype="multipart/form-data">
                                {!! csrf_field() !!}
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <span class="btn btn-primary btn-file bgm-gray1">
                                            <span class="fileinput-new">Seleccionar archivo para enviar</span>
                                            <span class="fileinput-exists">Canviar el archivo cargado</span>
                                            <input type="file" name="customers_dealers">
                                        </span>
                                        <span class="fileinput-filename"></span>
                                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                    </div>
                                </div>
                                
                                <div class="col-sm-4">
                                        <button type="submit" class="btn btn-primary btn-md bgm-gray1">Enviar</button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>

            <div class="card" style=" z-index: 1">

                        <div class="card-header">
                            <div class="row">
                                <div class="col-sm-6 col-md-6">
                                    <h2>Lista de clientes distribuidores <small>Lista completa de los clientes distribuidores disponibles.</small></h2>
                                </div>
                                <div class="col-sm-6 col-md-4 col-md-offset-2">
                                    <div class="mini-charts-item bgm-bluegray" style=" margin:0px;margin-top: 25px">
                                        <div class="clearfix">
                                            <div class="chart stats-line-2"></div>
                                            <div class="count">
                                                <small>Total clientes distribuidores</small>
                                                <h2>{!! $total !!}</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    

                    @if($customers_dealers->isEmpty())



                    <div class="container" style="text-align: center; padding-top: 60px; padding-bottom: 60px">

                        <h3>Actualmente no hay ningun cliente distribuidor registrado.</h3>
                    </div>

                    @else
                            
                        <table id="data-table-other" class="table table-striped table-vmiddle">
                            <thead>
                                <tr>
                                    <th data-column-id="id" data-type="numeric" data-width="7%">ID</th>
                                    <th data-column-id="dealer_id" data-width="9%">id distribuidor</th>
                                    <th data-column-id="customer_id" data-width="9%">id cliente</th>
                                    <th data-column-id="name" data-width="25%">nombre</th>
                                    <th data-column-id="nif" data-width="9%">NIF</th>
                                    <th data-column-id="city">Población</th>
                                    <th data-column-id="date" data-order="desc">Fecha creación</th>
                                    <th data-column-id="commands" data-formatter="commands" data-sortable="false">Opciones</th>

                                </tr>
                            </thead>
                            <tbody>

                            @foreach( $customers_dealers as $customer )
                                <tr>
                                    <td>{!! $customer->id !!}</td>
                                    <td>{!! $customer->dealer_id !!}</td>
                                    <td>{!! $customer->customer_id !!}</td>
                                    <td>{!! $customer->name !!}</td>
                                    <td>{!! $customer->NIF !!}</td>
                                    <td>{!! $customer->city !!}</td>
                                    <td>{!! $customer->created_at !!}</td>
                                </tr>
                            @endforeach
                                                 
                            </tbody>
                        </table>
                    @endif
                    </div>

            </div>
    </section>


@include('dashboard.layouts.footer')
